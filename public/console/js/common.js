if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search, this_len) {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}

$(document).ajaxSend(function(elm, xhr, s) {
    xhr.setRequestHeader('x-csrf-token', $('#csrf-token').attr("content"));
});

function isIE(userAgent) {
    userAgent = userAgent || navigator.userAgent;
    return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
}

function encode(str) {
    return btoa(str);
}

function rotate(img) {
    if (img.getAttribute('style') != null)
        img.removeAttribute('style');
    else
        img.setAttribute('style', 'transform:rotate(180deg)');
}

function getParam(id) {
    var e = document.getElementById(id);
    if (e == null)
        return "";
    if (e.type == "select-one")
        return "&" + id + "=" + encodeURIComponent(e.selectedIndex >= 0 ? e.options[e.selectedIndex].value : "");
    else if (e.type == "checkbox")
        return "&" + id + "=" + e.checked;
    else if (e.type == "number") {
        if (isNaN(parseFloat(e.value)))
            return "&" + id + "=0";
        else
            return "&" + id + "=" + parseFloat(e.value);
    } else {
        if (e.className == "dtpicker")
            return "&" + id + "=" + moment(e.value, "MM/DD/YYYY HH:mm:ss").unix() * 1000;
        else
            return "&" + id + "=" + encodeURIComponent(e.value);
    }
}

function getElementValue(id) {
    var e = document.getElementById(id);
    if (e == null)
        return "";
    if (e.type == "select-one")
        return encodeURIComponent(e.options[e.selectedIndex].value);
    else if (e.type == "checkbox")
        return e.checked;
    else {
        if (e.className == "dtpicker") {
            if (!!e.value)
                return moment(e.value, "MM/DD/YYYY HH:mm:ss").unix() * 1000;
            else
                return e.value;
        } else
            return encodeURIComponent(e.value);
    }
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function(m, key, value) {
            vars[key] = value;
        });
    return vars;
}

function getUrlParam(id) {
    var e = getUrlVars()[id];
    if (!e)
        return "";
    else
        return "&" + id + "=" + e;
}

function updateQuery(key, value) {
    window.location = updateUrlParameter(window.location.href, key, value);
}

function updateUrlParameter(uri, key, value) {
    // remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? '' : uri.substr(i);
    uri = i === -1 ? uri : uri.substr(0, i);

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";

    if (!value) {
        // remove key-value pair if value is empty
        uri = uri.replace(new RegExp("([?&]?)" + key + "=[^&]*", "i"), '');
        if (uri.slice(-1) === '?') {
            uri = uri.slice(0, -1);
        }
        // replace first occurrence of & by ? if no ? is present
        if (uri.indexOf('?') === -1) uri = uri.replace(/&/, '?');
    } else if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash;
}

function portraitClicked() {
    if (document.getElementById("signoutDiv").style.display !== "block") {
        document.getElementById("signoutDiv").style.display = "block";
    } else {
        document.getElementById("signoutDiv").style.display = "none";
    }
    if (event) event.stopPropagation();
}

function handleProtocolImage(imageURI, elementId) {
    $.ajax({
        type: "GET",
        contentType: "image/jpeg",
        url: "/console/getImageBase64/" + encode(imageURI),
        dataType: 'text',
        async: true,
        success: function(result) {
            if (!!result)
                document.getElementById(elementId).src = "data:image/jpeg;base64," + result;
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("ERROR: handleProtocolImage");
            document.getElementById(elementId).src = "/console/image/person-placeholder.jpg";
            return;
        }
    });
}

String.prototype.equalIgnoreCase = function(str) {
    return (str != null &&
        typeof str === 'string' &&
        this.toUpperCase() === str.toUpperCase());
}

String.prototype.toSnake = function() {
    if (this.length > 0)
        return this.charAt(0).toUpperCase() + this.slice(1);
    else
        return this;
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

function setCustomCookie(type, name) {
    setCookie("web_console_" + type + "_" + name, getElementValue(name), 365);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCustomCookie(type, name) {
    var value = getCookie("web_console_" + type + "_" + name);
    var e = document.getElementById(name);
    if (e != null && e.type == "checkbox") {
        if (value) {
            var boolValue = (/true/i).test(value);
            $('#' + name).prop("checked", boolValue).change();
        }
    }
    if (!!value) $('#' + name).val(value).change();
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function showLastN() {
    document.getElementById("lastNDiv").style.display = "block";
}

function hideLastN() {
    document.getElementById("lastNDiv").style.display = "none";
}

function changeLang() {
    var lang = document.getElementById("language").value;
    setCookie("lang", getElementValue("language"), 365);
    window.location.href = "/console/status?lang=" + lang;
}

function i18n(id) {
    if (id.indexOf(' ') >= 0)
        return id;
    var e = $("#" + id + "text");
    if (e.length > 0)
        return e.data("text");
    else {
        e = $("#" + id.toLowerCase() + "text");
        if (e.length > 0)
            return e.data("text");
        else
            return id;
    }
}

function findElements(tag, pattern) {
    var elArray = [];
    var tmp = document.getElementsByTagName(tag);

    var regex = new RegExp(pattern);
    for (var i = 0; i < tmp.length; i++) {

        if (regex.test(tmp[i].name)) {
            elArray.push(tmp[i]);
        }
    }

    return elArray;
}

$(window).on("load", function() {
    var urlLang = getUrlVars()["lang"];
    if (!!urlLang) {
        setCookie("lang", urlLang, 365);
        return;
    }
    var cookieLang = getCookie("lang");
    if (!!cookieLang && !cookieLang.equalIgnoreCase(i18n('locale')))
        updateQuery("lang", cookieLang);
    //else
    //updateQuery("lang", "en_US");
});

function copyClipboard(id) {
    $("#" + id).select();
    document.execCommand("Copy");
}

function isColor(strColor) {
    var s = new Option().style;
    s.color = strColor;
    return s.color == strColor;
}

function animateDots() {
    $("img[src$='dot.png']").eq(0).attr("class", "animate-flicker1");
    $("img[src$='dot.png']").eq(1).attr("class", "animate-flicker2");
    $("img[src$='dot.png']").eq(2).attr("class", "animate-flicker3");
}

function staticDots() {
    $("img[src$='dot.png']").attr("class", "dot");
}

function joinOption() {
    animateDots();
    window.location.href = "/console/join_option?1=1" + getUrlParam("host");
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function showMessageDialog(title, infoHtml) {
    $("#messageDialogTitle").text(title);
    $("#messageDialogInfo").html(infoHtml);
    $("#messageDialog").show();
}

function getWeekdayNumber(name) {
    switch (name) {
        case "sunday":
            return 0;
        case "monday":
            return 1;
        case "tuesday":
            return 2;
        case "wednesday":
            return 3;
        case "thursday":
            return 4;
        case "friday":
            return 5;
        case "saturday":
            return 6;
    }
}

function deleteCookies() {
    var allCookies = document.cookie.split(';');
    for (var i = 0; i < allCookies.length; i++)
        document.cookie = allCookies[i] + "=;expires=" +
        new Date(0).toUTCString();
}

function ConvertFormToJSON(form) {
    var array = jQuery(form).serializeArray();
    var json = {};

    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });

    return json;
}

function autoZoom() {
    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    var scale = Math.min(window.innerWidth / 1200, window.innerHeight / 800);
    if (isFirefox) {
        document.body.style.transform = "scale(" + scale + ")";
        let left = (window.innerWidth - $('.container')[0].getBoundingClientRect().width) / 2;
        document.body.style.cssText += "-moz-transform-origin: " + left + "px 20px;";
    } else {
        document.body.style.zoom = (scale * 100) + "%";
        document.body.style.marginTop = "20px";
    }

    document.body.style.height = "unset";
}