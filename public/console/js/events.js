var lastLoadDate = 0;
var eventFiltersToRemove = [];
var allEventFilters = [];
var faceSearchingInProgress = false;
var faceSearchingDisabled = false;
var searchByImage_placeHolderImage = new Image();
$(window).on('resize scroll', processImages);
$('document').ready(function() {
    localStorage.noResultstext = i18n('noResults');
    if ($("#noResultsDiv").length == 0) {
        var e = document.createElement("div");
        e.id = "noResultsDiv";
        e.innerHTML = '<div style="text-align:center;padding:50px;border:1px solid gray;">' + localStorage.noResultstext + '</div>';
        document.getElementById("list").appendChild(e);
    }
    localStorage.agetext = i18n('age');
    localStorage.maletext = i18n('male');
    localStorage.femaletext = i18n('female');
    localStorage.cannotaccesscamera = i18n('cannotaccesscamera');
    localStorage.unrecognizabletext = i18n('unrecognizable');
    localStorage.strangertext = i18n('stranger');
    localStorage.threattext = i18n('threat');
    localStorage.concerntext = i18n('concern');
    localStorage.durationtext = i18n('duration');
    localStorage.sitetext = i18n('site');
    localStorage.sourcetext = i18n('source');
    localStorage.smileToActivatetext = i18n('smileToActivate');
    localStorage.waitingtext = i18n('waiting');
    localStorage.badgeIdtext = i18n('badgeId');
    localStorage.directionOfTravelBytext = i18n('directionOfTravelBy');
    localStorage.directionOfTraveltext = i18n('directionOfTravel');
    localStorage.maskStatustext = i18n('maskStatus');
    localStorage.bodyTemptext = i18n('bodyTemp');
    localStorage.missingtext = i18n('missing');
    localStorage.lowtext = i18n('low');
    localStorage.hightext = i18n('high');
    localStorage.slightlyHightext = i18n('slightlyHigh');
    localStorage.veryHightext = i18n('veryHigh');
    localStorage.normaltext = i18n('normal');
    localStorage.locationtext = i18n('location');
    localStorage.eventtext = i18n('event');
    localStorage.eventstext = i18n('events');
    localStorage.searchByFacetext = i18n('searchByFace');
    localStorage.searchByFaceInEventstext = i18n('searchByFaceInEvents');
    localStorage.attributesNotUnderstoodtext = i18n('attributesNotUnderstood');
    localStorage.filterNotSupportedtext = i18n('filterNotSupported');
    localStorage.liveUpdatesOfftext = i18n('liveUpdatesOff');
    localStorage.rgbLivenessBytext = i18n('rgbLivenessBy');
    localStorage.poseLivenessBytext = i18n('poseLivenessBy');
    localStorage.bytext = i18n('by');
    localStorage.candidateListErrortext = i18n('candidateListError');
    localStorage.candidatetext = i18n('candidate');
    localStorage.candidatestext = i18n('candidates');
    localStorage.searchProgresstext = i18n('searchProgress');
    localStorage.eventIndexSearchStatstext = i18n('eventIndexSearchStats');
    localStorage.indexStartTimetext = i18n('indexStartTime');
    localStorage.indexEndTimetext = i18n('indexEndTime');
    localStorage.indexNewEventsSincetext = i18n('indexNewEventsSince');
    localStorage.availableIndextext = i18n('availableIndex');
    localStorage.eventBiometricIndexInUsetext = i18n('eventBiometricIndexInUse');
    localStorage.onlyOneCanBeIndexedtext = i18n('onlyOneCanBeIndexed');
    localStorage.contactAdminToSetuptext = i18n('contactAdminToSetup');
    localStorage.youCanChangeDirectorytext = i18n('youCanChangeDirectory');
    localStorage.changeIndexToCurrentDirectorytext = i18n('changeIndexToCurrentDirectory');
    localStorage.noIndexAvailabletext = i18n('noIndexAvailable');
    localStorage.indexNeedToBeSet1text = i18n('indexNeedToBeSet1');
    localStorage.searchtext = i18n('search');
    localStorage.upTotext = i18n('upTo');
    localStorage.setupEventIndexingtext = i18n('setupEventIndexing');
    localStorage.faceSearchTooManyRequestErrortext = i18n('faceSearchTooManyRequestError');
    localStorage.faceSearchUnknownErrortext = i18n('faceSearchUnknownError');
    localStorage.faceSearchNotFoundErrortext = i18n('faceSearchNotFoundError');
    localStorage.preparingtext = i18n('preparing');
    localStorage.selectSearchImagetext = i18n('selectSearchImage');
    localStorage.replaceSearchImagetext = i18n('replaceSearchImage');
    localStorage.clickFaceToSelecttext = i18n('clickFaceToSelect');
    localStorage.closeMatchtext = i18n('closeMatch');

    var dateRange = $('#dateRange').val();
    $('#dateRange').data('pre', dateRange);
    if ($('#sinceTime').data("DateTimePicker") == null) {
        $('#sinceTime').datetimepicker({ keepOpen: true, locale: getCookie("lang"), format: "MM/DD/YYYY HH:mm:ss" });
    }

    switch (dateRange) {
        case '5m':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(5, 'm'));
            break;
        case '15m':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(15, 'm'));
            break;
        case '1h':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(1, 'h'));
            break;
        case '24h':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(24, 'h'));
            break;
        case '48h':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(48, 'h'));
            break;
        case '7d':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(7, 'd'));
            break;
        case '30d':
            $('#sinceTime').data("DateTimePicker").date(moment().subtract(30, 'd'));
            break;
        case 'custom':
            $("#dateRange").append(new Option($('#sinceTime').val() + " - " + $('#untilTime').val(), "customDate"));
            $("#dateRange").val("customDate");
            break;
    }

    initSearchByImageUI();
    getCandidateListSize(false);

    $("#advancedSearch").css('display', getCookie("advancedSearch") || "none");
    document.getElementById('searchByImageOverlay').addEventListener("click", selectPeopleForSearch, false);
    window.addEventListener('scroll', scrollFunction);

    if (!!$('#eventId').val()) {
        $(".eventsUI").hide();
        loadEvent();
        return;
    }

    if ($('#advancedSearch').is(":visible"))
        $('#searchByImageThumb').hide();
    else
        $('#searchByImageThumb').show();

    if (!!sessionStorage["EVENT_FACE_SEARCH_DATA"]) {
        searchImageDataUrl(sessionStorage["EVENT_FACE_SEARCH_DATA"]);
        updateFaceSearchProgress(sessionStorage["EVENT_FACE_SEARCH_ID"]);
        return;
    } else {
        let dateRange = getUrlVars()["dateRange"];
        let sortBy = getUrlVars()["sortBy"] || "chronological";
        let offModeSortBys = ["name", "age", "duration"];
        if ("custom" == dateRange || offModeSortBys.includes(sortBy)) {
            console.log("Live update off mode, long polling not started...");
            $("#liveUpdateMode").text(localStorage.liveUpdatesOfftext);
            loadEvents(0);
        } else {
            console.log("Start long polling events update...");
            longPollingEvents();
        }
    }
});

function scrollFunction() {
    if (window.pageYOffset > 120) {
        $('#list').css("padding-top", 30 + $('#searchParams').height());
        $('#searchParams').addClass("sticky");
    } else {
        $('#searchParams').removeClass("sticky");
        $('#list').css("padding-top", 10);
    }
}

function processImages() {
    $('.portraitEvent').filter(':not(.done)')
        .each(function() {
            if (isInViewport(this.closest('.listItem100'))) {
                if (this.getAttribute("pid") != null && this.getAttribute("id") != null)
                    getPeopleImage(this.getAttribute("pid"), this.getAttribute("id"));
                else if (this.getAttribute("eid") != null && this.getAttribute("id") != null) {
                    getEventImage(this.getAttribute("eid"), this.getAttribute("id").split("_")[0], this.getAttribute("id"));
                }
            }
        });
}

function isInViewport(elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.bottom > 0 &&
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom - 5000 <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right - 100 <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function isChronAsc() {
    var sortBy = document.getElementById("sortBy");
    var order = document.getElementById("order");
    return sortBy != null && sortBy.selectedIndex == 0 && order.selectedIndex == 1;
}

function isChronDesc() {
    var sortBy = document.getElementById("sortBy");
    var order = document.getElementById("order");
    return sortBy != null && sortBy.selectedIndex == 0 && order.selectedIndex == 0;
}

function refreshEvent() {
    var url = "/console/events?action=refresh";
    url += getParam("name");
    url += getParam("groupBy");
    url += getParam("sortBy");
    url += getParam("order");
    url += getParam("site");
    url += getParam("gender");
    url += getParam("personType");
    if ($("#eventType").val() == "person")
        url += "&eventCategory=" + $("#eventType").val();
    else
        url += getParam("eventType");
    url += getParam("source");
    url += getParam("tenure");
    url += getParam("mask");
    url += getParam("idClass");

    var dateRange = $('#dateRange').val();
    if (dateRange == 'customDate' || dateRange == 'custom') {
        url += "&dateRange=custom";
        url += getParam("sinceTime");
        url += getParam("untilTime");
    } else {
        url += getParam("dateRange");
    }

    var ageRange = $('#ageRange').val();
    if (ageRange == 'custom' || ageRange == 'customAge') {
        url += "&ageRange=custom";
        url += getParam("minAge");
        url += getParam("maxAge");
    } else {
        url += getParam("ageRange");
    }

    url += getParam("shortestGap");
    url += getParam("shortestDuration");
    url += getParam("spanSources");
    window.location = url;
}

function dateChanged() {
    var dateRange = $('#dateRange').val();
    if (dateRange == '')
        return;
    if (dateRange == 'custom') {
        $('#timeRangeDiv').css('display', 'flex');
        return;
    }

    $('#timeRangeDiv').css('display', 'none');
    refreshEvent();
}

function ageChanged() {
    var ageRange = $('#ageRange').val();
    if (ageRange == 'custom') {
        $('#ageRangeDiv').css('display', 'flex');
        return;
    }

    $('#ageRangeDiv').css('display', 'none');
    refreshEvent();
}

function toggleAdvancedSearch() {
    var div = document.getElementById("advancedSearch");
    if ($('#advancedSearch').is(":visible")) {
        $('#advancedSearch').hide();
        $('#searchByImageThumb').show();
    } else {
        $('#advancedSearch').css('display', 'inline-block');
        $('#searchByImageThumb').hide();
    }
    setCookie("advancedSearch", $('#advancedSearch').css('display'), 365);
}

function searchByImage() {
    $("#searchByImageDiv").css("display", "flex");
    if (!$("#searchByImageFilePath")[0].files[0])
        $('#searchByImageFilePath').trigger('click');
}

function loadEvents(sinceModDate) {
    var url = "";
    $("#spinner").show();

    url += getParam("sinceTime");
    url += getParam("untilTime");

    var ageRange = $('#ageRange').val();
    $('#ageRange').data('pre', ageRange);
    switch (ageRange) {
        case '0-18':
            $('#minAge').val(0);
            $('#maxAge').val(18);
            break;
        case '18+':
            $('#minAge').val(18);
            $('#maxAge').val();
            break;
        case '18-29':
            $('#minAge').val(18);
            $('#maxAge').val(29);
            break;
        case '30-45':
            $('#minAge').val(30);
            $('#maxAge').val(45);
            break;
        case '45-65':
            $('#minAge').val(45);
            $('#maxAge').val(65);
            break;
        case '65+':
            $('#minAge').val(65);
            $('#maxAge').val();
            break;
        case '':
            $('#minAge').val();
            $('#maxAge').val();
            break;
        case 'custom':
            if ($('#maxAge').val() > 0) {
                if ($("#ageRange option[value='" + (($('#minAge').val() || 0) + "-" + $('#maxAge').val()) + "']").length == 0) {
                    $("#ageRange").append(new Option(($('#minAge').val() || 0) + " - " + $('#maxAge').val(), "customAge"));
                    $("#ageRange").val("customAge");
                } else {
                    $("#ageRange").val(($('#minAge').val() || 0) + "-" + $('#maxAge').val());
                }
            } else {
                if ($("#ageRange option[value='" + (($('#minAge').val() || 0) + "+") + "']").length == 0) {
                    $("#ageRange").append(new Option(($('#minAge').val() || 0) + "+", "customAge"));
                    $("#ageRange").val("customAge");
                } else {
                    $("#ageRange").val(($('#minAge').val() || 0) + "+");
                }
            }
            break;
    }

    url += getParam("name");
    url += getParam("groupBy");
    url += getParam("sortBy");
    url += getParam("order");
    url += getParam("site");
    url += getParam("gender");
    url += getParam("personType");
    if ($("#eventType").val() == "person")
        url += "&eventCategory=" + $("#eventType").val();
    else
        url += getParam("eventType");
    url += getParam("source");
    url += getParam("tenure");
    url += getParam("mask");
    url += getParam("idClass");
    url += "&minAge=" + $("#minAge").val();
    url += "&maxAge=" + $("#maxAge").val();
    url += getParam("shortestGap");
    url += getParam("shortestDuration");
    url += getParam("spanSources");
    url += "&count=" + (getUrlVars()["count"] || "5000");
    if (sinceModDate) {
        url += "&sinceModDate=" + sinceModDate;
    }
    let s0 = moment().unix();

    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getEvents?1=1" + url,
        dataType: 'json',
        success: function(result) {
            if (isSearchingByFace) {
                return;
            }
            console.log((sinceModDate || 'initial') + ' event count: ' + result.events.length);
            let s1 = moment().unix();
            console.log("getEvents: " + (s1 - s0) + " (second)");
            showEvents(result, false);
            console.log("showEvents: " + (moment().unix() - s1) + " (second)");
            $("#spinner").hide();
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (e.responseJSON == null || (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException"))
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
            else {
                //document.getElementById("listStatus").innerHTML = "Error loading Event list";
                $("#spinner").hide();
            }
        }
    });
}

function loadEvent() {
    var directory = $('#directory').val() || "main";
    var eventId = $('#eventId').val();

    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getEvents?sinceTime=0&eventId=" + eventId + "&directory=" + directory,
        dataType: 'json',
        success: function(result) {
            showEvents(result, false);
            $("#spinner").hide();
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (e.responseJSON == null || (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException"))
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
            else {
                //document.getElementById("listStatus").innerHTML = "Error loading Event list";
                $("#spinner").hide();
            }
        }
    });
}

function removeAllEndedEvents() {
    $('#list').children('.listItem100').each(function() {
        if ($(this).text().includes(localStorage.durationtext)) {
            $(this).remove();
        }
    });
}

function showEvents(response, searchByFaceResult) {
    var eventsDiv = document.getElementById("list");
    var events = response.events || [];
    var ii = events.length;
    let i = 0;

    while (i < ii) {
        var p = events[i];

        if (p == null) break;
        var e = document.createElement("div");
        e.id = "p_" + normalizeEventId(p.eventId);
        e.className = "listItem100 marginBottom10";

        if ($('#dateRange').val() == 'live') {
            if (!!p.endTime) {
                $('#' + e.id).remove();
                i++;
                continue;
            }
        }
        var idClassStyle = "purple";
        if ('recognizedObject'.equalIgnoreCase(p.type))
            idClassStyle = "gray";

        if (!!p.personId)
            idClassStyle = "blue";
        if (!!p.personId && !!p.name)
            idClassStyle = "lawngreen";

        if (!p.personId) {
            if ('unidentified'.equalIgnoreCase(p.idClass))
                idClassStyle = "gray";
            if (!p.idClass)
                idClassStyle = "purple";
        }

        if (p.similarityScore > 0 && p.similarityScore < 1)
            idClassStyle = "cyan";

        if ('threat'.equalIgnoreCase(p.idClass))
            idClassStyle = "red";
        else if ('concern'.equalIgnoreCase(p.idClass))
            idClassStyle = "orange";
        else if ('stranger'.equalIgnoreCase(p.idClass))
            idClassStyle = "purple";

        var html = "<div class=\"inline eventRow\">";
        html += "<div class=\"\"><label class=\"checkboxLabel\"><input type=\"checkbox\" class=\"check\" tag=\"root\" id=\"cb_" + p.eventId + "\" onclick=\"event.cancelBubble=true;checkboxClicked('" + p.eventId + "');\"/></label></div>";

        if ('person'.equalIgnoreCase(p.type)) {
            html += getEventImageHtml(p, "face", idClassStyle, true);
            html += getPeopleImageHtml(p, idClassStyle);
            html += getEventRowSummary(p, idClassStyle);
            html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
        } else if ('action'.equalIgnoreCase(p.type)) {
            if ('smileToActivate'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "nosmile", idClassStyle, true);
                html += getEventImageHtml(p, "smile", idClassStyle, true);
                html += getPeopleImageHtml(p, idClassStyle);
                html += getIconImage("smile");
                html += getEventRowSummary(p, idClassStyle);
            } else if ('rgbLiveness'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "face", idClassStyle, true);
                html += getPeopleImageHtml(p, idClassStyle);
                html += getIconImage("liveness" + (p.livenessConfirmed == true ? "True" : "False"));
                html += getEventRowSummary(p, idClassStyle);
            } else if ('poseLiveness'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "faceCenter", idClassStyle, true);
                html += getEventImageHtml(p, "faceProfile", idClassStyle, true);
                html += getPeopleImageHtml(p, idClassStyle);
                html += getIconImage("poseLiveness");
                html += getEventRowSummary(p, idClassStyle);
            } else if ('directionOfTravel'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "best", idClassStyle, true);
                html += getPeopleImageHtml(p, idClassStyle);
                if (!!p.directionId)
                    html += getIconImage(p.directionId);
                else
                    html += getIconImage("wrongDirection");
                html += getEventImageHtml(p, "trigger", idClassStyle, false);
                html += getEventRowSummary(p, idClassStyle);
                html += getEventImageHtml(p, "sceneTrigger", idClassStyle, false);
            } else if ('maskCheck'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "face", idClassStyle, true);
                html += getEventRowSummary(p, idClassStyle);
                html += getPeopleImageHtml(p, idClassStyle);
            } else if ('faceTempMeasurement'.equalIgnoreCase(p.actionId)) {
                html += getEventImageHtml(p, "face", idClassStyle, true);
                html += getEventRowSummary(p, idClassStyle);
                html += getPeopleImageHtml(p, idClassStyle);
            }
        } else if ('object'.equalIgnoreCase(p.type)) {
            html += getEventImageHtml(p, "face", idClassStyle, false);
            html += getIconImage("object");
            html += getEventRowSummary(p, idClassStyle);
            html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
        } else if ('tag'.equalIgnoreCase(p.type)) {
            html += getEventImageHtml(p, "face", idClassStyle, false);
            html += getIconImage("tag");
            html += getEventRowSummary(p, idClassStyle);
            html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
        } else if ('recognizedObject'.equalIgnoreCase(p.type)) {
            if ('person'.equalIgnoreCase(p.objectType) || ('common'.equalIgnoreCase(p.objectType) && 'person'.equalIgnoreCase(p.objectId))) {
                if (true == p.hasSubEvents) {
                    if ('noconcern'.equalIgnoreCase(p.idClass) ||
                        'concern'.equalIgnoreCase(p.idClass) ||
                        'threat'.equalIgnoreCase(p.idClass)) {
                        //face detail
                        html += getEventImageHtml(p, "faceDetail", idClassStyle, true);
                        //body
                        html += getEventImageHtml(p, "face", idClassStyle, false);
                        //ref face
                        html += getPeopleImageHtml(p, idClassStyle);
                        html += getEventRowSummary(p, idClassStyle);
                        html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
                    } else {
                        //face detail
                        html += getEventImageHtml(p, "faceDetail", idClassStyle, true);
                        //body
                        html += getEventImageHtml(p, "face", idClassStyle, false);
                        html += getEventRowSummary(p, idClassStyle);
                        html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
                    }
                } else {
                    //body
                    html += getEventImageHtml(p, "face", idClassStyle, false);
                    html += getEventRowSummary(p, idClassStyle);
                    html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
                }
            } else {
                html += getEventImageHtml(p, "face", idClassStyle, false);
                html += getEventRowSummary(p, idClassStyle);
                html += getEventImageHtml(p, "sceneThumb", idClassStyle, false);
            }
        } else {
            html += getEventImageHtml(p, "face", idClassStyle, false);
            html += getEventRowSummary(p, idClassStyle);
        }

        html += "</div>";
        e.innerHTML = html;
        var o = document.getElementById(e.id);
        if (searchByFaceResult == false && isSearchingByFace == true)
            return;
        if (o) {
            //for liveness action, the icon might change, so need to reload the whole row.
            /*
                        if('rgbLiveness'.equalIgnoreCase(p.actionId)) {
                            o.parentNode.replaceChild(e, o);
                        }
                        else {
                            o.getElementsByClassName("summary")[0].outerHTML = summaryHtml;
                            let imgs = o.getElementsByClassName("portraitEvent");
                            for(let g=0;g<imgs.length;g++)
                                imgs[g].classList.remove("done");
                            let frames = o.getElementsByClassName("portrait");
                            for(let f=0;f<frames.length;f++) {
                                frames[f].setAttribute("class", "portrait " + idClassStyle);
                            }
                        }
            */

            let imgs = o.getElementsByClassName("portraitEvent");
            let newImgs = e.getElementsByClassName("portraitEvent");
            for (let g = 0; g < imgs.length; g++) {
                if (newImgs.length > g)
                    newImgs[g].src = imgs[g].src;
            }

            o.parentNode.replaceChild(e, o);
        } else {
            if (isChronDesc() && !isSearchingByFace)
                eventsDiv.insertBefore(e, eventsDiv.childNodes[0]);
            else
                eventsDiv.appendChild(e);
        }
        ++i;
    }

    if ($(".listItem100").length == 0) {
        if ($("#noResultsDiv").length == 0) {
            var e = document.createElement("div");
            e.id = "noResultsDiv";
            e.innerHTML = '<div style="text-align:center;padding:50px;border:1px solid gray;">' + localStorage.noResultstext + '</div>';
            eventsDiv.appendChild(e);
        }
    } else {
        $('#noResultsDiv').remove();
    }

    $("#spinner").hide();
    processImages();

    if ($('#dateRange').val() == 'live') {
        removeAllEndedEvents();
    }

    resultCount();
}

function resultCount() {
    var newCount = $('.listItem100').length;
    $('#count').text(newCount);
    $('#countLable').text(newCount == 1 ? localStorage.eventtext : localStorage.eventstext);
}

function getIconImage(type) {
    return "<div class=\"portrait gray\"><img class=\"portraitEvent\" src=\"/console/image/icon-" + type + ".png\"/></div>";
}

function getEventImageHtml(p, type, idClassStyle, needSearchIcon) {
    if (type == "sceneThumb") idClassStyle += " sceneThumb";
    let html = "<div onmouseover=\"mouseOverFace(this, " + needSearchIcon + ")\" onmouseout=\"mouseOutFace(this)\" class=\"portrait " + idClassStyle + "\"><img class=\"portraitEvent\" id=\"" + type + "_" + normalizeEventId(p.eventId) + "\" eid=\"" + encode(p.eventId) + "\"/>";
    if (needSearchIcon === true) html += "<div class=\"tooltipDiv\" ><img class=\"searchIcon\" src=\"/console/image/search-face.png\"/><div class=\"tooltiptext1\"><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImage(this)\">" + localStorage.searchByFacetext + "</div><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImageInEvents(this)\">" + localStorage.searchByFaceInEventstext + "</div></div></div>";
    html += "</div>";
    return html;
}

function getPeopleImageHtml(p, idClassStyle) {
    if (!!p.personId) {
        let html = "<div pid=\"" + p.personId + "\" onmouseover=\"mouseOverFace(this, true)\" onmouseout=\"mouseOutFace(this)\" class=\"hidden portrait " + idClassStyle + "\"><img class=\"portraitEvent\" id=\"person_" + normalizeEventId(p.eventId) + "\" pid=\"" + p.personId + "\" />";
        html += "<div class=\"tooltipDiv\" ><img class=\"searchIcon\" src=\"/console/image/search-face.png\"/><div class=\"tooltiptext1\"><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImage(this)\">" + localStorage.searchByFacetext + "</div><div class=\"menuItem\" onclick=\"event.cancelBubble=true;searchFaceImageInEvents(this)\">" + localStorage.searchByFaceInEventstext + "</div></div></div>";
        html += "</div>";
        return html;
    } else {
        return "";
    }
}

function getEventRowSummary(p, idClassStyle) {
    var html = "<div class=\"summary\">";
    if (p.eventSimilarityScore != null) {
        var similarScoreBGColor = "#267BB6";
        if ('threat'.equalIgnoreCase(p.idClass)) {
            similarScoreBGColor = "red";
        } else if ('concern'.equalIgnoreCase(p.idClass)) {
            similarScoreBGColor = "orange";
        }
        let eventSimilarityScore = (Math.floor(p.eventSimilarityScore * 100));
        if (eventSimilarityScore > 100) eventSimilarityScore = 100;
        html += "<span class=\"similarityScore\" style=\"background-color:" + similarScoreBGColor + "\">" + eventSimilarityScore + "%</span>";
    }
    //line1
    if ('maskCheck'.equalIgnoreCase(p.actionId)) {
        html += "<span class=\"inline marginRight10\">" + localStorage.maskStatustext + ":</span>";
        html += p.mask ? ("<span class=\"inline marginRight10\"><img class=\"smallIcon\" src=\"/console/image/masked.png\"/></span>") : ("<span class=\"inline marginRight10\">" + localStorage.missingtext + "</span>");
    } else if (p.mask) html += "<span class=\"inline marginRight10\"><img class=\"smallIcon\" src=\"/console/image/masked.png\"/></span>";
    else if (!!p.occlusion && p.occlusion > 0.5) html += "<span class=\"inline marginRight10\"><img class=\"smallIcon\" title=\"" + p.occlusion + "\" src=\"/console/image/occlusion.png\"/></span>";
    if ('faceTempMeasurement'.equalIgnoreCase(p.actionId)) {
        html += "<span class=\"inline marginRight10\">" + localStorage.bodyTemptext + ":</span>";
        let tempColor = "green";
        let tempName = "normal";
        if (p.measuredTemp < 35.5) {
            tempColor = "#6554C1";
            tempName = localStorage.lowtext;
        } else if (p.measuredTemp >= 35.5 && p.measuredTemp <= 37.5) {
            tempColor = "green";
            tempName = localStorage.normaltext;
        } else if (p.measuredTemp >= 37.5 && p.measuredTemp < 38) {
            tempColor = "#FF991F";
            tempName = localStorage.slightlyHightext;
        } else if (p.measuredTemp >= 38 && p.measuredTemp < 39.5) {
            tempColor = "#FF991F";
            tempName = localStorage.hightext;
        } else if (p.measuredTemp >= 39.5) {
            tempColor = "#FF5630";
            tempName = localStorage.veryHightext;
        }
        html += "<span class=\"inline marginRight10\" style=\"color:" + tempColor + "\">" + tempName + " " + p.measuredTemp + "Â° C</span>";
    }
    if ('recognizedObject'.equalIgnoreCase(p.type)) {
        if ('unidentified'.equalIgnoreCase(p.idClass) || !p.idClass) {
            if ('person'.equalIgnoreCase(p.objectType) || ('common'.equalIgnoreCase(p.objectType) && 'person'.equalIgnoreCase(p.objectId))) {
                html += "<span class=\"inline marginRight10 " + idClassStyle + "\">" + localStorage.unrecognizabletext + "</span>";
            }
            if (!!p.objectId) html += "<span class=\"inline marginRight10\">" + p.objectId + "</span>";
        } else {
            if (!'noconcern'.equalIgnoreCase(p.idClass))
                html += "<span class=\"inline\">" + i18n(p.idClass) + "</span>";
            if (!!p.name) html += "<span class=\"inline\">" + p.name + "</span>";
            else if (!!p.objectId && !'person'.equalIgnoreCase(p.objectId)) html += "<span class=\"inline\">" + p.objectId + "</span>";
        }
    } else {
        if (!!p.actionId) html += "<span class=\"inline marginRight10\">" + p.actionId + "&nbsp;" + (!!p.name ? localStorage.bytext : "") + "</span>";
        if ('person'.equalIgnoreCase(p.type) && 'purple'.equalIgnoreCase(idClassStyle)) html += "<span class=\"inline marginRight10 purple\">" + localStorage.strangertext + "</span>";
        if ('person'.equalIgnoreCase(p.type) && 'red'.equalIgnoreCase(idClassStyle)) html += "<span class=\"inline marginRight10 red\">" + localStorage.threattext + (!!p.name ? ":" : "") + "</span>";
        if ('person'.equalIgnoreCase(p.type) && 'orange'.equalIgnoreCase(idClassStyle)) html += "<span class=\"inline marginRight10 orange\">" + localStorage.concerntext + (!!p.name ? ":" : "") + "</span>";
        if ('person'.equalIgnoreCase(p.type) && 'gray'.equalIgnoreCase(idClassStyle)) html += "<span class=\"inline marginRight10 gray\">" + localStorage.unrecognizabletext + " person</span>";
        if (p.similarityScore > 0 && p.similarityScore < 1) html += "<span class=\"inline marginRight10\">" + localStorage.closeMatchtext + ": " + (Math.floor(p.similarityScore * 100)) + "%: </span>";
        if (!!p.name) html += "<span class=\"inline marginRight10\">" + p.name + "</span>";
        else { if (!!p.personId) html += "<span class=\"inline marginRight10\">" + p.personId + "</span>"; }

        if (!!p.objectId) html += "<span class=\"inline marginRight10\">" + p.objectId + "</span>";
        if (!!p.tagId) html += "<span class=\"inline marginRight10\">" + localStorage.badgeIdtext + ":&nbsp;" + p.tagId + "</span>";
    }
    //html += "<br>";

    //line2
    html += "<span class=\"inline marginRight10\">";
    var line2 = "";
    if (!!p.tagType) line2 += p.tagType + ",&nbsp;";
    if (!'recognizedObject'.equalIgnoreCase(p.type)) {
        if (!!p.objectType) line2 += p.objectType + ",&nbsp;";
    }

    if (p.livenessConfirmed == true)
        html += "<span class=\"inline\"><img class=\"smallIcon\" src=\"/console/image/icon-livenessTrue.png\"/></span>";
    else if (p.livenessConfirmed == false) {
        html += "<span class=\"inline\"><img class=\"smallIcon\" src=\"/console/image/icon-livenessFalse.png\"/></span>";
    }

    if ('male'.equalIgnoreCase(p.gender)) line2 += localStorage.maletext + ",&nbsp;"
    else if ('female'.equalIgnoreCase(p.gender)) line2 += localStorage.femaletext + ",&nbsp;"
    if (!!p.age) line2 += localStorage.agetext + "&nbsp;" + Math.round(p.age) + ",&nbsp;";
    if (line2.endsWith(",&nbsp;")) line2 = line2.substring(0, line2.length - 7);
    html += line2 + "</span>";
    if (!!p.sentiment) {
        var smallIcon = "";
        if (p.sentiment < 0)
            smallIcon = "<img class=\"smallIcon\" src=\"/console/image/sad.png\"/>"
        else if (p.sentiment >= 0 && p.sentiment <= 40) {
            smallIcon = "<img class=\"smallIcon\" src=\"/console/image/neutral.png\"/>"
        } else {
            smallIcon = "<img class=\"smallIcon\" src=\"/console/image/happy.png\"/>"
        }
        html += "<span class=\"inline\">" + smallIcon + p.sentiment + ",&nbsp;</span>";
    }
    if (!!p.smilePercentage) {
        var smallIcon = "<img class=\"smallIcon\" src=\"/console/image/smile.png\"/>"
        html += "<span class=\"inline\">" + smallIcon + p.smilePercentage + "%</span>";
    }
    if (!!p.personType) html += "<span class=\"inline\">" + p.personType + "</span>";

    html += "<br>";

    //line3
    var duration = (p.durationString || "").replace('Duration', localStorage.durationtext).replace('Waiting', localStorage.waitingtext);
    if ('action'.equalIgnoreCase(p.type))
        html += "<span class=\"inlineUnset\">" + (p.endTime == undefined ? "" : moment(p.endTime).format("MM/DD/YYYY hh:mm:ss A")) + "</span>";
    else
        html += "<span class=\"inlineUnset\">" + moment(p.startTime).format("MM/DD/YYYY hh:mm:ss A") + " - " + (p.endTime == undefined ? "" : moment(p.endTime).format("MM/DD/YYYY hh:mm:ss A")) + " &nbsp;" + duration + "</span>";

    if (p.city || p.countyName || p.geoRegionName || p.countryName) {
        let locString = "at " + (p.city ? (p.city + ", ") : "") + (p.countyName ? (p.countyName + ", ") : "") + (p.geoRegionName ? (p.geoRegionName + ", ") : "") + (p.countryName ? (p.countryName) : "");
        locString = locString.replace(/,\s*$/, "");
        html += "&nbsp;<span class=\"inlineUnset\">" + locString + "</span>";
    }
    html += "<br>";

    //line4
    if (!!p.siteId) html += "<span class=\"sitesource\" title=\"" + (p.siteId || "") + "\">" + localStorage.sitetext + ":&nbsp;" + (p.siteId || "") + "</span>";
    if (!!p.sourceId) html += "<span class=\"sitesource\" title=\"" + (p.sourceId || "") + "\">" + localStorage.sourcetext + ":&nbsp;" + (p.sourceId || "") + "</span>";
    if (!!p.latitude && !!p.longitude) html += "<span class=\"inlineUnset\">" + localStorage.locationtext + ":&nbsp;" + p.latitude + "Â° N&nbsp;&nbsp;" + p.longitude + "Â° W</span>";
    if (!!p.personId) html += "<br><a name=\"" + p.personId + "\" href=\"javascript:editPeople('" + p.personId + "')\">View Person Record</a>";
    html += "</div>"
    return html;
}

function getEventImage(eventId, type, elementId) {
    $('#' + elementId).addClass('done');
    var unknownPerson_src = "/console/image/person-placeholder.jpg";
    let hideNoneExistTypes = ["sceneThumb", "trigger", "sceneTrigger"];

    var directory = $('#directory').val() || "";

    $.ajax({
        type: "GET",
        tryCount: 0,
        retryLimit: 1,
        contentType: "image/jpeg",
        url: "/console/getEventImage/" + eventId + "/" + type + "?directory=" + directory,
        dataType: 'text',
        async: true,
        success: function(result) {
            if (!!result) {
                $('#' + elementId).attr("src", "data:image/jpeg;base64," + result).addClass('done');
                //$('#'+elementId).parent().show();
            } else {
                if (hideNoneExistTypes.includes(type))
                    $('#' + elementId).parent().hide();
                else {
                    $('#' + elementId).attr("src", unknownPerson_src);
                    $('#' + elementId).parent().show();
                }
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("ERROR: getEventImage, will retry.");
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
                $.ajax(this);
            } else {
                if (hideNoneExistTypes.includes(type))
                    document.getElementById(elementId).parentNode.style.display = "none";
                else
                    $('#' + elementId).attr("src", unknownPerson_src);
            }
            return;
        }
    });
}

function getPeopleImage(personId, elementId) {
    //check if current element is processed
    if ($('#' + elementId).hasClass('done'))
        return;

    //Process for all images with the same personId, instead of processing once for each occurrence
    $('[pid=' + personId + ']').addClass('done');

    var directory = $('#directory').val() || "";

    $.ajax({
        type: "GET",
        tryCount: 0,
        retryLimit: 1,
        contentType: "image/jpeg",
        url: "/console/getPeopleImage/" + personId + "?directory=" + directory,
        dataType: 'text',
        async: true,
        success: function(result) {
            if (!!result) {
                $('[pid=' + personId + ']').attr("src", "data:image/jpeg;base64," + result);
                $('div[pid=' + personId + ']').removeClass("hidden");
            } else
                $('a[name ="' + personId + '"]').show();
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("ERROR: getPeopleImage, will retry.");
            $('a[name ="' + personId + '"]').hide();
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
                $.ajax(this);
            }
            return;
        }
    });
}

function longPollingEvents() {
    (function poll() {
        $.ajax({
            url: "/console/eventStatus?since=" + lastLoadDate, //document.getElementById("lastLoadDate").getAttribute('value'),
            success: function(response) {
                if (isSearchingByFace) {
                    return;
                }
                loadEvents(lastLoadDate);
                if (lastLoadDate == response.lastModDate)
                    setTimeout(function() {}, 1000);
                lastLoadDate = response.lastModDate > 0 ? response.lastModDate : response.serverDate;
            },
            complete: function(event, xhr, settings) {
                if (isSearchingByFace) {
                    console.log("Searching by face: events polling cancelled;");
                    return;
                }
                if (xhr == "error")
                    setTimeout(function() { poll(); }, 10000);
                else
                    poll();
            },
            dataType: "json",
            timeout: 30000
        });
    })();
}

function deleteAllEvents() {
    var msg = $("#deleteAllEventsButton").data("text");
    if (!confirm(msg))
        return;

    var url = "";
    url += getParam("name");
    url += getParam("groupBy");
    url += getParam("sortBy");
    url += getParam("order");
    url += getParam("site");
    url += getParam("gender");
    url += getParam("personType");
    url += getParam("source");
    url += getParam("tenure");
    url += getParam("mask");
    url += getParam("idClass");
    url += getParam("sinceTime");
    url += getParam("untilTime");
    url += getParam("shortestGap");
    url += getParam("shortestDuration");
    url += getParam("spanSources");
    url += "&minAge=" + $("#minAge").val();
    url += "&maxAge=" + $("#maxAge").val();
    if ($("#eventType").val() == "person")
        url += "&eventCategory=" + $("#eventType").val();
    else
        url += getParam("eventType");

    $.ajax({
        type: "DELETE",
        contentType: "application/json;charset=UTF-8",
        url: "/console/deleteEvents?1=1" + url,
        data: { _method: 'delete' },
        async: false,
        success: function() {
            window.location.reload();
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException")
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
            else {
                //document.getElementById("listStatus").innerHTML = "Error loading Event list";
            }
        }
    });
}

function mouseOverFace(x, compare) {
    x.firstChild.style.width = "auto";
    if (compare) {
        x.firstChild.style.height = "160px";
    } else {
        var body = document.body,
            html = document.documentElement;
        x.firstChild.style.height = "auto";
        let iHeight = x.firstChild.height;
        x.firstChild.style.height = "99px";
        var height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        if (x.getBoundingClientRect().y + window.scrollY + Math.max(iHeight, 160) > height)
            x.firstChild.style.marginTop = (100 - Math.max(iHeight, 160)) + "px";

        x.firstChild.style.height = "auto";
        if (x.firstChild.height < 160)
            x.firstChild.style.height = "160px";
    }
    x.firstChild.style.zIndex = "2";
    let hasScore = x.parentNode.getElementsByClassName("summary")[0].getElementsByClassName("similarityScore").length == 1;

    if (isSearchingByFace && hasScore && compare) {
        var cmp = document.createElement("img");
        cmp.id = "compareImage";
        cmp.className = "compareImage";
        cmp.style.position = "absolute";
        cmp.style.left = x.firstChild.width + "px";
        cmp.style.height = "160px";
        cmp.style.top = "0";
        cmp.style.zIndex = "3";
        cmp.src = document.getElementById('searchByImageFace').toDataURL('image/jpeg');
        x.appendChild(cmp);
        var compareScore = x.parentNode.getElementsByClassName("summary")[0].children[0].cloneNode(true);
        compareScore.id = "compareScore";
        compareScore.style.position = "absolute";
        compareScore.style.left = (x.firstChild.width - 15) + "px";
        compareScore.style.top = "0";
        compareScore.style.zIndex = "4";
        if (x.firstChild.attributes["pid"] != undefined) {
            let canvas = document.getElementById('searchByImageFace');
            $.ajax({
                type: "POST",
                contentType: "application/json;charset=UTF-8",
                url: "/console/verification/" + x.firstChild.attributes["pid"].value,
                async: false,
                data: canvas.toDataURL('image/jpeg'),
                dataType: "json",
                success: function(result) {
                    if (result.identifiedFaces == null || result.identifiedFaces.length < 1)
                        return;
                    let similarityScore = (Math.floor(result.identifiedFaces[0].similar[0].similarityScore * 100));
                    if (similarityScore > 100) similarityScore = 100;
                    var similarScoreBGColor = "#267BB6";
                    compareScore.innerHTML = "<span class=\"similarityScore\" style=\"background-color:#267BB6\">" + similarityScore + "%</span>";
                },
                error: function(e) {
                    console.log("ERROR: ", e);
                }
            });
        }
        x.appendChild(compareScore);
    }
    if (x.firstChild.src.startsWith("data:image") && x.getElementsByClassName('searchIcon').length > 0) {
        x.getElementsByClassName('searchIcon')[0].style.zIndex = 3;
        x.getElementsByClassName('searchIcon')[0].style.display = "block";
    }
}

function mouseOutFace(x) {
    $('#compareImage').remove();
    $('#compareScore').remove();
    x.firstChild.style.marginTop = "0";
    x.firstChild.style.height = "99px";
    x.firstChild.style.width = "100px";
    x.firstChild.style.zIndex = "";
    if (x.getElementsByClassName('searchIcon').length > 0) {
        x.getElementsByClassName('searchIcon')[0].style.zIndex = 1;
        x.getElementsByClassName('searchIcon')[0].style.display = "none";
    }
}

function editPeople(personId) {
    var directory = $('#directory').val() || "";

    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getPeople?personId=" + personId + "&directory=" + directory,
        dataType: 'json',
        timeout: 100000,
        success: function(result) {
            showPeopleEditor(result);
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException")
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
        }
    });
}

function showPeopleEditor(data) {
    $('input[type=submit]', this).attr('disabled', null);
    var editor = document.getElementById("editor");
    var unknownPerson_src = "/console/image/person-placeholder.jpg";
    editor.style.display = "block";
    if (data.imageURI != null && (data.imageURI.indexOf("cvos://") == 0 || data.imageURI.indexOf("ehttps://") == 0)) {
        handleProtocolImage(data.imageURI, "editor_portrait");
    } else {
        document.getElementById("editor_portrait").src = (data.imageURI || unknownPerson_src);
    }

    var idClassStyle = "gray";
    if ('threat'.equalIgnoreCase(data.idClass))
        idClassStyle = "threat";
    else if ('concern'.equalIgnoreCase(data.idClass))
        idClassStyle = "concern";
    document.getElementById("editor_portrait").className = "portrait " + idClassStyle;

    document.getElementById("editor_name").value = data.name;
    document.getElementById("editor_personId").value = data.personId;
    document.getElementById("editor_enrolledDate").innerHTML = data.enrollmentDate;
    document.getElementById("editor_gender").value = data.gender || "unknown";
    document.getElementById("editor_age").value = (data.age) ? Math.round(data.age) : "";
    document.getElementById("editor_company").value = data.company;
    document.getElementById("editor_moniker").value = data.moniker;
    document.getElementById("editor_phone").value = data.validationPhone;
    document.getElementById("editor_email").value = data.validationEmail;
    document.getElementById("editor_idClass").value = data.idClass;
    document.getElementById("editor_homeLocation").value = data.homeLocation;
    if (!!data.expDate)
        $('#editor_enrollmentExp').data("DateTimePicker").date(moment(data.expDate));
    else
        $('#editor_enrollmentExp').data("DateTimePicker").clear();
    document.getElementById("editor_personType").value = data.personType;
    document.getElementById("editor_externalId").value = data.externalId;
    document.getElementById("editor_tags").value = data.allTags;
}

function cancelUpdatePeople() {
    var editor = document.getElementById("editor");
    editor.style.display = "none";
}

function normalizeEventId(id) {
    return id.replace("=", "");
}

function checkboxClicked(eventId) {
    var event = document.getElementById("p_" + eventId);
    updateEventsMenu();
}

function updateEventsMenu() {
    var y = document.getElementsByClassName("check");
    var anyChecked = false;
    for (i = 0; i < y.length; i++) {
        var p = y[i];
        if (p.checked) {
            anyChecked = true;
            break;
        }
    }

    var stickyMenu = 54;
    var menu = document.getElementById("eventsMenu");
    if (window.pageYOffset + $(window).height() + stickyMenu > $(document).height()) {
        menu.classList.remove("sticky2");
    } else {
        menu.classList.add("sticky2");
    }

    if (anyChecked) {
        $("#deleteAllEventsButton").prop("disabled", true);
        document.getElementById("eventsMenu").style.display = "block";
    } else {
        $("#deleteAllEventsButton").prop("disabled", false);
        document.getElementById("eventsMenu").style.display = "none";
    }
}

function cancelSelectEvents() {
    $("#deleteAllEventsButton").prop("disabled", false);
    var x = document.getElementsByClassName("check");
    for (i = 0; i < x.length; i++) {
        var p = x[i];
        p.checked = false;
    }
    document.getElementById("eventsMenu").style.display = "none";
}

function deleteSelectedEvents() {
    var msg = $("#deleteSelectedEvents").data("text");
    if (!confirm(msg))
        return;

    $('#list input:checked').each(function() {
        let eventId = $(this).attr('id').replace("cb_", "");
        deleteEvents(eventId);
    });

    location.reload();
}

function deleteEvents(eventId) {
    var directory = $('#directory').val() || "";

    $.ajax({
        type: "DELETE",
        url: "/console/deleteEvents?eventId=" + eventId + "&directory=" + directory,
        data: { _method: 'delete' },
        async: false,
        success: function(result) {
            $('#cb_' + eventId).remove();
        },
        error: function(e) {
            console.log("ERROR: ", e);
            if (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException")
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
        }
    });
}

function searchFaceImage(e) {
    let sid_P = uuidv4();
    sessionStorage[sid_P] = e.parentNode.parentNode.parentNode.firstChild.src;
    location.href = "/console/people?sid_P=" + sid_P;
}

function searchFaceImageInEvents(e) {
    sessionStorage["EVENT_FACE_SEARCH_DATA"] = e.parentNode.parentNode.parentNode.firstChild.src;
    sessionStorage.removeItem("EVENT_FACE_SEARCH_ID");
    window.location.reload();
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function setCustomDateRange() {
    window.stop();
    $('#timeRangeDiv').hide();
    refreshEvent();
}

function filterIconClicked() {
    if (document.getElementById("filterDiv").style.display !== "block") {
        document.getElementById("filterDiv").style.display = "block";
    } else {
        document.getElementById("filterDiv").style.display = "none";
    }
    if (event) event.stopPropagation();
}

function populateEventFilters() {
    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getPreferences?name=eventFilters",
        dataType: 'json',
        success: function(config) {
            allEventFilters = config.filters;
            $("#filterListDiv").html("");
            if (!config || !allEventFilters || allEventFilters.length == 0) {
                var filterElement = document.createElement("div");
                filterElement.id = "noFilterPresetExists";
                filterElement.className = "menuItem";
                filterElement.innerHTML = i18n("noFilterPresetExists");
                $("#filterListDiv").append(filterElement);
            }
            for (let i = 0; i < allEventFilters.length; i++) {
                let filter = allEventFilters[i];
                var filterElement = document.createElement("div");
                filterElement.className = "menuItem";
                filterElement.setAttribute("data", JSON.stringify(filter));
                filterElement.addEventListener("click", function(event) {
                    //let f = JSON.parse(event.toElement.attributes["data"].value);
                    applyFilterSearch(filter);
                });
                filterElement.innerHTML = filter.filterName;
                $("#filterListDiv").append(filterElement);
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            var filterElement = document.createElement("div");
            filterElement.id = "noFilterPresetExists";
            filterElement.className = "menuItem";
            filterElement.innerHTML = i18n("noFilterPresetExists");
            $("#filterListDiv").append(filterElement);
        }
    });
}

function addPresetFilter() {
    for (let j = 0; j < allEventFilters.length; j++) {
        if ($('#filterName').val() == allEventFilters[j].filterName) {
            $('#addFilterDialogMessage').text(i18n("filterNameAlreadyExists"));
            return;
        }
    }
    $('#addFilterDialogMessage').text('');
    $('#noFilterPresetExists').remove();
    var newFilter = new Object();

    newFilter.filterName = $('#filterName').val();

    newFilter.date = new Object();
    var dateRange = $('#dateRange').val();
    switch (dateRange) {
        case '5m':
            newFilter.date.lastMilliseconds = 300000;
            break;
        case '15m':
            newFilter.date.lastMilliseconds = 900000;
            break;
        case '1h':
            newFilter.date.lastMilliseconds = 3600000;
            break;
        case '24h':
            newFilter.date.lastMilliseconds = 86400000;
            break;
        case '48h':
            newFilter.date.lastMilliseconds = 172800000;
            break;
        case '7d':
            newFilter.date.lastMilliseconds = 604800000;
            break;
        case '30d':
            newFilter.date.lastMilliseconds = 2592000000;
            break;
        case 'live':
            newFilter.date.lastMilliseconds = 0;
            break;
        case 'custom':
        case 'customDate':
            newFilter.date.sinceTime = $('#sinceTime').data("DateTimePicker").date().unix() * 1000;
            newFilter.date.untilTime = $('#untilTime').data("DateTimePicker").date().unix() * 1000;
            break;
    }

    newFilter.idClasses = [];
    newFilter.idClasses.push($("#idClass").val());

    newFilter.sites = [];
    if (!!$("#site").val())
        newFilter.sites.push('#' + $("#site").val());

    newFilter.sources = [];
    if (!!$("#source").val())
        newFilter.sources.push('#' + $("#source").val());

    newFilter.eventTypes = [];
    newFilter.eventTypes.push($("#eventType").val());

    newFilter.personName = $("#name").val();

    newFilter.personTypes = [];
    newFilter.personTypes.push($("#personType").val().replace('$absent', 'none').replace('$present', 'any'));

    newFilter.gender = [];
    newFilter.gender.push($("#gender").val());

    newFilter.age = new Object();
    if (!!$("#minAge").val())
        newFilter.age.min = parseInt($("#minAge").val());
    if (!!$("#maxAge").val())
        newFilter.age.max = parseInt($("#maxAge").val());

    newFilter.tenure = [];
    newFilter.tenure.push($("#tenure").val());

    newFilter.mask = [];
    newFilter.mask.push($("#mask").val());

    //newFilter.groups = [];
    //newFilter.groups.push($("#group").val());

    newFilter.shortestGap = $("#shortestGap").val() * 1000;
    newFilter.shortestDuration = $("#shortestDuration").val() * 1000;

    newFilter.spanSources = $("#spanSources").val() == "true";

    newFilter.sort = new Object();
    newFilter.sort.by = $("#sortBy").val();
    newFilter.sort.order = $("#order").val();

    allEventFilters.push(newFilter);
    let request = new Object;
    request.filters = allEventFilters;
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        url: "/console/setPreferences?name=eventFilters",
        data: JSON.stringify(request),
        success: function(result) {
            var filterElement = document.createElement("div");
            filterElement.className = "menuItem";
            filterElement.setAttribute("data", JSON.stringify(newFilter));
            filterElement.addEventListener("click", function(event) {
                //let f = JSON.parse(event.toElement.attributes["data"].value);
                applyFilterSearch(newFilter);
            });
            filterElement.innerHTML = newFilter.filterName;
            $("#filterListDiv").append(filterElement);
            $('#filterName').val("");
            $('#addFilterButton').addClass("disabled");
            $("#addFilterDialog").hide();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert(localStorage.errorOccurredtext);
            window.location.reload();
        }
    });
}

function showManageFilterDialog() {
    $("#manageFilterDialog").show();
    $("#editEventFiltersDiv").html("");
    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getPreferences?name=eventFilters",
        dataType: 'json',
        success: function(config) {
            if (!config || !config.filters || config.filters.length == 0) {
                $("#editEventFiltersDiv").css("text-align", "center").text(i18n("noFilterPresetExists"))
                return;
            } else {
                $("#editEventFiltersDiv").css("text-align", "unset");
            }
            allEventFilters = config.filters;
            eventFiltersToRemove = [];
            for (let i = 0; i < allEventFilters.length; i++) {
                let filter = allEventFilters[i];
                var eid = uuidv4();
                var filterElement = document.createElement("div");
                filterElement.id = eid;
                filterElement.className = "filterItem";
                var html = '<input data-value="' + filter.filterName + '" value="' + filter.filterName + '" /></input><a href="javascript:removeFilterItem(\'' + filter.filterName + '\',\'' + eid + '\');">X</a>';
                filterElement.innerHTML = html;
                $("#editEventFiltersDiv").append(filterElement);
            }
        },
        error: function(xhr, textStatus, errorThrown) {}
    });
}

function updatePresetFilters() {
    for (let i = 0; i < eventFiltersToRemove.length; i++) {
        for (let j = 0; j < allEventFilters.length; j++) {
            if (eventFiltersToRemove[i] == allEventFilters[j].filterName) {
                allEventFilters.splice(j, 1);
                break;
            }
        }
    }

    $("#editEventFiltersDiv input").each(function() {
        if ($(this).data('value') != $(this).val()) {
            var filter = allEventFilters.find(a => { return a.filterName === $(this).data('value') && a.renamed == undefined });
            if (filter != null) {
                filter.filterName = $(this).val();
                filter.renamed = true;
                console.log($(this).data('value') + " -> " + $(this).val());
            }
        }
    });

    for (let i = 0; i < allEventFilters.length; i++) {
        delete allEventFilters[i].renamed;
    }

    let request = new Object;
    request.filters = allEventFilters;

    $.ajax({
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        url: "/console/setPreferences?name=eventFilters",
        data: JSON.stringify(request),
        success: function(result) {
            populateEventFilters();
            $("#manageFilterDialog").hide();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert(localStorage.errorOccurredtext);
            populateEventFilters();
            $("#manageFilterDialog").hide();
        }
    });
}

function addFilterSubmitEnableCheck() {
    if (isEmptyOrSpaces($('#filterName').val())) {
        $('#addFilterButton').addClass("disabled");
    } else {
        $('#addFilterButton').removeClass("disabled");
    }
}

function applyFilterSearch(newFilter) {
    if (!newFilter || !newFilter.filterName)
        alert(localStorage.errorOccurredtext);
    var unsupportedFields = [];

    var url = "/console/events?action=refresh";
    if (newFilter.personName != null) url += "&name=" + newFilter.personName;
    if (newFilter.date != null) {
        if (newFilter.date.lastMilliseconds != null) {
            switch (newFilter.date.lastMilliseconds) {
                case 300000:
                    url += "&dateRange=5m";
                    break;
                case 900000:
                    url += "&dateRange=15m";
                    break;
                case 3600000:
                    url += "&dateRange=1h";
                    break;
                case 86400000:
                    url += "&dateRange=24h";
                    break;
                case 172800000:
                    url += "&dateRange=48h";
                    break;
                case 604800000:
                    url += "&dateRange=7d";
                    break;
                case 2592000000:
                    url += "&dateRange=30d";
                    break;
                case 0:
                    url += "&dateRange=live";
                    break;
                default:
                    unsupportedFields.push("lastMilliseconds");
                    break;
            }
        } else {
            url += "&dateRange=custom";
            url += "&sinceTime=" + newFilter.date.sinceTime;
            url += "&untilTime=" + newFilter.date.untilTime;
            if (newFilter.date.sinceTime == null || newFilter.date.untilTime == null) {
                unsupportedFields.push("sinceTime");
                unsupportedFields.push("untilTime");
            }
        }
    }
    if (newFilter.idClasses != null) url += "&idClass=" + newFilter.idClasses;

    if (newFilter.sites != null && newFilter.sites.toString().startsWith("#")) url += "&site=" + newFilter.sites.toString().substring(1);
    if (newFilter.sources != null && newFilter.sources.toString().startsWith("#")) url += "&source=" + newFilter.sources.toString().substring(1);

    if (newFilter.eventTypes != null) {
        if (newFilter.eventTypes[0] == "person") url += "&eventCategory=" + newFilter.eventTypes;
        else url += "&eventType=" + newFilter.eventTypes;
    }
    if (newFilter.personTypes != null) url += "&personType=" + newFilter.personTypes.toString().replace('any', '$present').replace('none', '$absent');

    if (newFilter.gender != null) url += "&gender=" + newFilter.gender;
    if (newFilter.age != null && (newFilter.age.min != null || newFilter.age.max != null)) {
        url += "&ageRange=custom";
        url += "&minAge=" + (newFilter.age.min || "");
        url += "&maxAge=" + (newFilter.age.max || "");
    }
    if (newFilter.tenure != null) url += "&tenure=" + newFilter.tenure.toString().toLowerCase();
    if (newFilter.mask != null) url += "&mask=" + newFilter.mask;
    //if(newFilter.groups != null) url += "&group="+newFilter.groups;
    if (newFilter.shortestGap >= 0) url += "&shortestGap=" + (newFilter.shortestGap / 1000);
    if (newFilter.shortestDuration >= 0) url += "&shortestDuration=" + (newFilter.shortestDuration / 1000);
    if (newFilter.spanSources != null) url += "&spanSources=" + newFilter.spanSources;
    if (newFilter.sort != null) {
        url += "&sortBy=" + (newFilter.sort.by || "");
        url += "&order=" + (newFilter.sort.order || "");
    }
    if (unsupportedFields.length > 0)
        showConfigError(unsupportedFields);
    else
        window.location = url;
}

function showConfigError(fields) {
    var message = localStorage.attributesNotUnderstoodtext + ":<br><ul>";
    for (let i = 0; i < fields.length; i++) {
        message += "<li>" + fields[i] + "</li>";
    }
    message += "</ul>";
    showMessageDialog(localStorage.filterNotSupportedtext, message);
}

function removeFilterItem(filterName, eid) {
    $("#" + eid).remove();
    eventFiltersToRemove.push(filterName);
}


function postFaceSearch(data) {
    updateEventIndexingStatus();
    isSearchingByFace = true;
    faceSearchUIReset();

    $("#searchByImageThumb").attr("src", data);
    sessionStorage["EVENT_FACE_SEARCH_DATA"] = data;
    updateFaceSearchProgress(sessionStorage["EVENT_FACE_SEARCH_ID"]);
}

function startFaceSearch() {
    let canvas = document.getElementById('searchByImageFace');
    let data = canvas.toDataURL('image/jpeg');
    isSearchingByFace = true;

    $('#dateRange').addClass('disabled');
    $('#site').addClass('disabled');
    $('#source').addClass('disabled');
    $('#selectImageBtn').hide();
    $('#clearSearchImageBtn').hide();
    $('#listSizeConfigBtn').hide();
    document.getElementById('searchByImageParam').removeAttribute("onclick");
    $('#searchByImageParam').unbind('click');
    $('#searchByImageFace').css('cursor', 'default');

    let similarLimit = (parseInt($('#numberOfCandidatesToDisplay').val()) || 100) + (parseInt($('#maximumAdditionalCandidatesToDisplay').val()) || 100);
    $("#spinner").show();
    $.ajax({
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        url: "/console/postEventBioIndexFaceSearch?1=1" + getParam("sinceTime") + getParam("untilTime") + "&similarLimit=" + similarLimit + getParam("site") + getParam("source"),
        data: data,
        dataType: "json",
        success: function(result) {
            if (result.searchId != null) {
                sessionStorage["EVENT_FACE_SEARCH_DATA"] = data;
                sessionStorage["EVENT_FACE_SEARCH_ID"] = result.searchId;
                faceSearchingInProgress = true;
                updateFaceSearchProgress(result.searchId);
            } else {
                alert(localStorage.errorOccurredtext);
            }
        },
        error: function(e) {
            $("#spinner").hide();
            console.log("ERROR: ", e);
            if (!!e.responseText)
                alert(i18n(e.responseText));
            faceSearchUIReset();
            updateEventIndexingStatus();
            if (e.responseJSON != null && e.responseJSON.message == "SessionTimeOutException")
                window.location.href = "/console/signin?url=" + encodeURIComponent(window.location.href);
        }
    });
}

function updateLastEventQueryState() {
    $.ajax({
        type: "POST",
        url: "/console/updateLastEventQueryState" + window.location.search,
        error: function(event, xhr, settings) {}
    });
}

function updateFaceSearchProgress(searchId) {
    if (!searchId) return;

    $('#dateRange').addClass('disabled');
    $('#site').addClass('disabled');
    $('#source').addClass('disabled');
    $('#selectImageBtn').hide();
    $('#clearSearchImageBtn').hide();
    $('#listSizeConfigBtn').hide();
    document.getElementById('searchByImageParam').removeAttribute("onclick");
    $('#searchByImageParam').unbind('click');
    $('#searchByImageFace').css('cursor', 'default');

    $('#headerForSearch').show();
    $('#headerForEvents').hide();
    (function poll() {
        $.ajax({
            url: "/console/getEventBioIndexFaceSearchById?searchId=" + searchId, //document.getElementById("lastLoadDate").getAttribute('value'),
            success: function(response) {
                $('#headerForSearchL').html("<span style='position:absolute;left:20px;line-height:31px;'>" + localStorage.searchProgresstext + ": " + response.percentProcessed + "%</span><progress class=normalProgress max=100 value=" + response.percentProcessed + "></progress>");
                $('#headerForSearchR').html();
                $('#clearFaceSearchResultBtn').css('visibility', 'visible');
                if (response.status == "processed" || response.percentProcessed >= 100) {
                    if (response.events != null && response.events.length > 0) {
                        filterFaceSearchResultsAndShow(response);
                    } else {
                        $('#headerForSearchL').html("<span>0 " + localStorage.candidatestext + "</span>");
                        $("#spinner").hide();
                        if ($("#list").html() == "") {
                            var e = document.createElement("div");
                            e.id = "noResultsDiv";
                            e.innerHTML = '<div style="text-align:center;padding:50px;border:1px solid gray;">' + localStorage.noResultstext + '</div>';
                            document.getElementById("list").appendChild(e);
                        }
                    }
                } else {
                    if (faceSearchingInProgress)
                        setTimeout(function() { poll(); }, 250);
                }
            },
            error: function(event, xhr, settings) {
                clearFaceSearchResult(false);
                if (faceSearchingInProgress)
                    switch (event.responseText) {
                        case "TOO_MANY_REQUESTS":
                            alert(localStorage.faceSearchTooManyRequestErrortext);
                            break;
                        case "UNKNOWN_ERROR":
                            alert(localStorage.faceSearchUnknownErrortext);
                            break;
                        case "NOT_FOUND":
                            alert(localStorage.faceSearchNotFoundErrortext);
                            break;
                        default:
                            setTimeout(function() { poll(); }, 10000);
                    }
            },
            dataType: "json",
            timeout: 2000
        });
    })();
}

function filterFaceSearchResultsAndShow(response) {
    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getPreferences?name=searchCandidateList",
        async: true,
        dataType: 'json',
        success: function(config) {
            if (config) {
                $('#numberOfCandidatesToDisplay').val(config.minimumCandidates);
                $('#minimumSimilarityForAdditionalCandidates').val(config.minimumSimilarityForAdditionalCandidates * 100);
                $('#maximumAdditionalCandidatesToDisplay').val(config.maximumAdditionalCandidates);
            }
        },
        complete: function() {
            if (faceSearchingDisabled == true) {
                $('#headerForSearchL').html('');
                return;
            }
            let minimumSimilarityScore = parseInt($('#minimumSimilarityForAdditionalCandidates').val() || 75) / 100;
            let numberOfCandidatesToDisplay = (parseInt($('#numberOfCandidatesToDisplay').val()) || 100);
            let similarLimit = numberOfCandidatesToDisplay + (parseInt($('#maximumAdditionalCandidatesToDisplay').val()) || 100);

            for (i = numberOfCandidatesToDisplay; i < response.events.length; i++) {
                if (response.events[i].eventSimilarityScore < minimumSimilarityScore)
                    response.events.splice(i);
            }
            $('#headerForSearchL').html("<span>" + response.events.length + " " + (response.events.length == 1 ? localStorage.candidatetext : localStorage.candidatestext) + "</span>");
            showEvents(response, true);
        }
    });
}

function listSizeConfigCheck() {
    if (($('#numberOfCandidatesToDisplay').val() || 0) == 0 && ($('#maximumAdditionalCandidatesToDisplay').val() || 0) == 0)
        $('#listSizeConfigSubmitBtn').addClass('disabled');
    else
        $('#listSizeConfigSubmitBtn').removeClass('disabled');

    if ($('#numberOfCandidatesToDisplay').val() > 1000)
        $('#numberOfCandidatesToDisplay').val(1000);
    if ($('#maximumAdditionalCandidatesToDisplay').val() > 1000)
        $('#maximumAdditionalCandidatesToDisplay').val(1000);
    if ($('#minimumSimilarityForAdditionalCandidates').val() > 100)
        $('#minimumSimilarityForAdditionalCandidates').val(100);
    if ($('#numberOfCandidatesToDisplay').val() < 0)
        $('#numberOfCandidatesToDisplay').val(0);
    if ($('#maximumAdditionalCandidatesToDisplay').val() < 0)
        $('#maximumAdditionalCandidatesToDisplay').val(0);
    if ($('#minimumSimilarityForAdditionalCandidates').val() < 0)
        $('#minimumSimilarityForAdditionalCandidates').val(0);
}

function getCandidateListSize(show) {
    $.ajax({
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        url: "/console/getPreferences?name=searchCandidateList",
        dataType: 'json',
        success: function(config) {
            if (config) {
                $('#numberOfCandidatesToDisplay').val(config.minimumCandidates);
                $('#minimumSimilarityForAdditionalCandidates').val(config.minimumSimilarityForAdditionalCandidates * 100);
                $('#maximumAdditionalCandidatesToDisplay').val(config.maximumAdditionalCandidates);
            }
            if (show == true) {
                listSizeConfigCheck();
                $("#listSizeConfigDialog").show();
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            //alert(localStorage.candidateListErrortext);
            listSizeConfigCheck();
            if (show == true) $("#listSizeConfigDialog").show();
        }
    });
}

function setCandidateListSize() {
    var candidateList = new Object();

    candidateList.minimumCandidates = $('#numberOfCandidatesToDisplay').val() || 100;
    candidateList.minimumSimilarityForAdditionalCandidates = ($('#minimumSimilarityForAdditionalCandidates').val() || 75) / 100;
    candidateList.maximumAdditionalCandidates = $('#maximumAdditionalCandidatesToDisplay').val() || 100;

    $.ajax({
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        url: "/console/setPreferences?name=searchCandidateList",
        data: JSON.stringify(candidateList),
        success: function(result) {
            $("#listSizeConfigDialog").hide();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert(localStorage.candidateListErrortext);
        }
    });
}

function faceSearchUIReset() {
    $('.searchParams').removeClass("sticky");
    $('.eventsMenu').removeClass("sticky2");
    $('.eventSearchParam').addClass("disabled").val('');
    $('.menuDiv').addClass("disabled").hide();
    $('.faceSearchParam').removeClass("disabled");
    $("#list").html("");
    $('#headerForSearch').show();
    $('#headerForEvents').hide();
    $('#headerForSearchL').html("");
    $('#headerForSearchR').html("");
    $('#dateRange').removeClass('disabled');
    $('#site').removeClass('disabled');
    $('#source').removeClass('disabled');
    $('#selectImageBtn').show();
    $('#clearSearchImageBtn').show();
    $('#listSizeConfigBtn').show();
    $('#searchByImageParam').on("click", searchByImage);
    $('#searchByImageFace').css('cursor', 'pointer');
    $('#clearFaceSearchResultBtn').css('visibility', 'hidden');

    if ($('#advancedSearch').is(":visible"))
        $('#searchByImageThumb').hide();
    else
        $('#searchByImageThumb').show();
}

function clearFaceSearchResult(reload) {
    faceSearchingInProgress = false;
    faceSearchUIReset();
    if ($('#advancedSearch').is(":visible"))
        $('#searchByImageThumb').hide();
    else
        $('#searchByImageThumb').show();

    let sid_E = sessionStorage["EVENT_FACE_SEARCH_ID"];
    if (!!sid_E) {
        $.ajax({
            type: "DELETE",
            contentType: "application/json;charset=UTF-8",
            url: "/console/deleteEventBioIndexFaceSearch?searchId=" + sid_E,
        });
    }
    sessionStorage.removeItem("EVENT_FACE_SEARCH_ID");
    if (reload)
        window.location.reload();
}

function showEventIndexingConfigDialog() {
    $('#eventIndexingConfigDialog').show();
    eventIndexingConfigChanged();
}

function eventIndexingConfigChanged() {
    if ($('#onlyIndexEventsOccuredAfterSpecificDate').is(":checked")) {
        $('#onlyIndexEventsOccuredAfterSpecificDateDiv').show();
    } else {
        $('#onlyIndexEventsOccuredAfterSpecificDateDiv').hide();
    }
    $('#eventIndexingSubmit').removeClass('disabled');
}

function submitEventIndexingConfigUpdate(json) {
    $.ajax({
        type: "POST",
        url: "/console/updateEventIndexingConfig",
        data: JSON.stringify(json),
        contentType: "application/json;charset=UTF-8",
        dataType: 'text',
        async: true,
        success: function(data) {
            if (!!data) {
                $("#eventIndexingDialogMessage").text(i18n(data)).addClass('red');
            } else {
                window.location.reload();
            }
        },
        error: function(data) {
            if (!!data && !!data.responseText) {
                obj = JSON.parse(data.responseText);
                if (!!obj && !!obj.error)
                    $("#eventIndexingDialogMessage").text(i18n(obj.error)).addClass('red');
                else
                    $("#eventIndexingDialogMessage").text(i18n("eventIndexingCouldNotBeConfigured")).addClass('red');
            } else {
                $("#eventIndexingDialogMessage").text(i18n("eventIndexingCouldNotBeConfigured")).addClass('red');
            }
        }
    });
}

function updateEventIndexingConfigClicked() {
    $('#eventIndexingForm').unbind('submit').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var json = ConvertFormToJSON(form);
        if (!$('#immediateForNewEvents').is(":checked")) {
            json.immediateForNewEvents = false;
        } else {
            json.immediateForNewEvents = true;
        }
        if (!$('#onlyIndexEventsOccuredAfterSpecificDate').is(":checked")) {
            json.sinceEventTime = 0;
        } else {
            json.sinceEventTime = moment($('#indexingSinceEventTime').val()).unix() * 1000;
        }
        submitEventIndexingConfigUpdate(json);
    });
}

function updateEventIndexingStatus() {
    let check = $('#addFilterDialog').length == 1;
    var directory = $('#directory').val() || "main";
    (function poll() {
        $.ajax({
            type: "GET",
            contentType: "application/json;charset=UTF-8",
            url: "/console/getEventIndexingStatus",
            dataType: 'json',
            timeout: 2000,
            tryCount: 0,
            retryLimit: 1,
            success: function(result) {
                $("#spinner").hide();
                let html = "";
                if (result != null) {
                    html = "<div class='tooltipDiv' style='position:relative;' ><span>" + localStorage.availableIndextext + ":&nbsp;</span>";
                    if (result.enabled == true && result.status != "unprocessed" && result.processedUpToEventTime > 0) {
                        if (result.directory == directory) {
                            faceSearchingDisabled = false;
                            if (result.processedFromEventTime > 0)
                                html += "<span>" + (result.processedFromEventTime > 0 ? moment(result.processedFromEventTime).format("MMM DD YYYY") : "") + " - " + (result.processedUpToEventTime > 0 ? moment(result.processedUpToEventTime).format("MMM DD YYYY") : "") + "</span>";
                            else
                                html += "<span>" + localStorage.upTotext + " " + (result.processedUpToEventTime > 0 ? moment(result.processedUpToEventTime).format("MMM DD YYYY") : "") + "</span>";

                            html += "<span class='tooltiptext1' style='text-align:left;top:20px;left:unset;right:0;'>" + localStorage.eventIndexSearchStatstext + ":<br>";

                            if (result.processedFromEventTime > 0) html += localStorage.indexStartTimetext + ": " + moment(result.processedFromEventTime).format("MMM DD YYYY") + "<br>";
                            html += localStorage.indexEndTimetext + ": " + moment(result.processedUpToEventTime).format("MMM DD YYYY") + "<br>";
                            if (result.configModDate > 0) html += localStorage.indexNewEventsSincetext + " " + moment(result.configModDate).format("MMM DD YYYY") + "</span></div>";
                            $('#searchByFaceBtn').removeClass('disabled');
                            html += "<img id='clearFaceSearchResultBtn' src=\"/console/image/x-white.png\" style='visibility:visible;' onclick='clearFaceSearchResult(true);'/>"
                        } else {
                            faceSearchingDisabled = true;
                            clearFaceSearchResult(false);
                            $('#searchByFaceBtn').addClass('disabled');
                            if (check)
                                $('#list').html('<div style="text-align:center;padding:50px;border:1px solid gray;color:red;">' + localStorage.eventBiometricIndexInUsetext + '<br>' + localStorage.onlyOneCanBeIndexedtext + '<br>' + localStorage.youCanChangeDirectorytext.replace('"main"', '"' + directory + '"') + '<br><br><button type="button" class="mediumButton" onclick="showEventIndexingConfigDialog();">' + localStorage.changeIndexToCurrentDirectorytext + '</button></div>');
                            else
                                $('#list').html('<div style="text-align:center;padding:50px;border:1px solid gray;color:red;">' + localStorage.eventBiometricIndexInUsetext + '<br>' + localStorage.onlyOneCanBeIndexedtext + '<br><br>' + localStorage.contactAdminToSetuptext + '</div>');

                            html = "<div class='redStroked'>" + localStorage.noIndexAvailabletext + "</div>";
                        }
                    } else {
                        faceSearchingDisabled = true;
                        clearFaceSearchResult(false);
                        $('#searchByFaceBtn').addClass('disabled');
                        if (result.enabled != true) {
                            html += "<b class='redStroked'>" + localStorage.noIndexAvailabletext + "</b></div>";
                            if (check) {
                                $('#list').html('<div style="text-align:center;padding:50px;border:1px solid gray;color:red;">' + localStorage.indexNeedToBeSet1text.replace('"main"', '"' + directory + '"') + '<br><br><button type="button" class="mediumButton" onclick="showEventIndexingConfigDialog();">' + localStorage.setupEventIndexingtext + '</button></div>');
                            } else {
                                $('#list').html('<div style="text-align:center;padding:50px;border:1px solid gray;color:red;">' + localStorage.indexNeedToBeSet1text.replace('"main"', '"' + directory + '"') + '<br><br>' + localStorage.contactAdminToSetuptext + '</div>');
                            }
                        } else {
                            html += "<b>" + localStorage.preparingtext + "...</b></div>";
                            $("#spinner").show();
                        }
                        setTimeout(function() { poll(); }, 2000);
                    }
                    $('#headerForSearchR').html(html);

                    if (!$('#headerForSearchL').text() && !faceSearchingDisabled) {
                        $('#headerForSearchL').html("<button id='searchByFaceBtn' onclick='startFaceSearch();'><span style='vertical-align:top;line-height: 26px;'>" + localStorage.searchtext + "</span><img src=\"/console/image/icon-search.png\" style='vertical-align:sub;width:20px;margin-left:10px;'/></button>");
                        $('#clearFaceSearchResultBtn').css('visibility', 'hidden');
                    }
                } else {
                    faceSearchingDisabled = true;
                    clearFaceSearchResult(false);
                    $('#searchByFaceBtn').addClass('disabled');
                    html = "<div class='redStroked'>" + localStorage.noIndexAvailabletext + "</div>";
                    $('#headerForSearchR').html(html);
                    setTimeout(function() { poll(); }, 2000);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                $('#eventIndexingStatusAdditional').hide();
                $('.eventIndexingConfigStatus').hide();
                setTimeout(function() { poll(); }, 2000);
            }
        });
    })();
}