function saveSettings(ele) {
    var settings = [];
    var type = $(ele).attr("setting-type");
    $('.' + type + ' input').each(function(i, item) {
        var sKey = $(item).attr("key");
        settings.push({ name: sKey, value: item.value });
        //console.log(item.value + "  " + $(item).attr("key"));
    });
    ajaxCallSaveSettings(settings, type);
}

function setSettingType(ele) {
    var type = $(ele).attr("setting-type");
    $("#settingsModalLabel center").text($(ele).attr("settings-modal-label"));
    $('.database-settings').addClass("d-none");
    $("." + type).removeClass("d-none");
    $("#save-settings").attr("setting-type", type);
}

//save-settings
//setting-modal

function ajaxCallSaveSettings(data, type) {
    $.ajax({
        url: "/save-settings/" + type,
        type: "POST",
        data: data,
        // async: false,
        success: function(result) {
            $("#settingsModal").modal("toggle");
            alert("Settings Saved Successfully");
            location.reload();
        }
    });
}