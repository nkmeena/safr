<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Safr - Dashboard</title>
<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200&display=swap" rel="stylesheet"> 
<!-- Custom styles for this template-->
<link href="css/admin.min.css" rel="stylesheet">
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<script>

function getANPRModalData(ele){

  var LicenseNum =  $(ele).attr('LicenseNum');
  var EventTime =  $(ele).attr('EventTime');
  var Location =  $(ele).attr('Location');
  var LaneName =  $(ele).attr('LaneName');
  var CameraID =  $(ele).attr('CameraID');
  var LPImage =  $(ele).attr('LPImage');
  var Make =  $(ele).attr('make');
  var Color =  $(ele).attr('color');

  //startTime 
  $("#LicenseNum").text(LicenseNum);
  $("#EventTime").text(EventTime);
  $("#Location").text(Location);
  $("#CameraID").text(CameraID);
  $("#LaneName").text(LaneName);
  $("#Make").text(Make);
  $("#Color").text(Color);
  //$("#LPImage").text(idClass);
  $("#LPImage").attr("src", LPImage);

    $("#anprTransactionModal").modal("toggle");
}

</script>



</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    @include("real-networks.sidebar.main-menu")
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <!-- START of Topbar -->
      @include("real-networks.header.navbar")
       <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <!-- Face Recognition -->
            <!-- <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Face Recognition</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/frs-icon.png">
                    </div>
                  </div>

                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div> -->

            <!-- ANPR -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">ANPR</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/anpr-icon.png">
                    </div>
                  </div>

                  <div class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" id="searchNumber" class="form-control border-0" placeholder="Type Vechile Number...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Crowd Monitoring -->
            <!-- <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Video Analytics</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/video-analysis.png">
                    </div>
                  </div>
                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div> -->
          </div> 
          <div class="dynamic-data">
           <div class="datetime_ratio_data">
              @foreach ($datetime_ratio as $key=>$value )
              <input key="{{$key}}" type="text"  value="{{$value}}" class="hidden d-none">
              @endforeach
            </div>
          </div>
          <!-- Content Row -->
          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-uppercase">ANPR Analytics</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-uppercase">ANPR Transactions</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="anprDataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Vehicle Number</th>
                          <th>Date/Time</th>
                          <th>Location</th>
                          <th>Lane Name</th>
                          <th>Camera ID</th>
                          <th>Color</th>
                          <th>Make</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($transaction_list as $event)
                        <?php 
                            $epoch = ceil( data_get($event  ,'TimeStamp' , 0));
                            
                            $hours = "-";
                            if(!empty($epoch)){
                              $dt = new DateTime("@$epoch");  // convert UNIX timestamp to PHP DateTime
                            //  $hours =  $dt->format('Y-m-d H:i:s'); // output = 2017-01-01 00:00:00
                            $dt->add(new DateInterval('PT5H30M'));
                            $hours = $dt->format('Y-m-d h:i A');// output = 2017-01-01 00:00:00

                            }
                            ?>
                          <tr>
                            <td>{{data_get($event  ,'LicenseNum' , "-")}}</td>
                            <td>{{$hours}}</td>
                            <td>{{data_get($event , 'Location' , "-")}}</td>
                            <td>{{data_get($event , 'LaneName' , "-")}}</td>
                            <td>{{data_get($event , 'CameraID' , "-")}}</td>
                            <td>{{data_get($event , 'color' , "-")}}</td>
                            <td>{{data_get($event , 'make' , "-")}}</td>
                            
                            <td><button   class="btn btn-dark btn-sm"  onclick="getANPRModalData(this)"
                            LicenseNum="{{data_get($event  ,'LicenseNum' , "-")}}" 
                            EventTime="{{$hours}}"
                            LicenseNum="{{data_get($event  ,'LicenseNum' , "-")}}"
                            Location="{{data_get($event  ,'Location' , "-")}}"
                            LaneName="{{data_get($event  ,'LaneName' , "-")}}"
                            CameraID="{{data_get($event  ,'CameraID' , "-")}}"
                            Make="{{data_get($event  ,'make' , "-")}}"
                            Color="{{data_get($event  ,'color' , "-")}}"
                            LPImage="{{data_get($event  ,'TransactionFiles.LPImage' , "-")}}">Details</button></td>
                          </tr>
                        @endforeach
                        
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div><!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>© 2021 RealNetworks, Inc. All Rights Reserved</span>
          </div>
        </div>
      </footer><!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="settingsModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
  
   <!-- The Modal -->
   <div class="modal fade" id="anprTransactionModal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">ANPR Transaction Detail</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <!-- <img src="cinqueterre.jpg" id ="personIMage" class="rounded" alt="Person Image"> -->
          <table class="table table-striped">
            <tbody>

             
      

              <tr>
                <td>Vehicle Number</td>
                <td id="LicenseNum">-</td>
              </tr>

              <tr>
                <td>Date/Time</td>
                <td id="EventTime">-</td>
              </tr>

              <tr>
                <td>Location</td>
                <td id="Location">-</td>
              </tr>

              <tr>
                <td>Lane Name</td>
                <td id="LaneName">-</td>
              </tr>

              <tr>
                <td>Camera ID</td>
                <td id="CameraID">-</td>
              </tr>

              <tr>
                <td>Make</td>
                <td id="Make">-</td>
              </tr>

              <tr>
                <td>Color</td>
                <td id="Color">-</td>
              </tr>


              <tr>
                <td>LP Image</td>
                <td > <img width="200" height="80" src="#" id ="LPImage" class="rounded" alt="LP Image"> </td>
              </tr>

            


            </tbody>
          </table>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
  @include("real-networks.popup.settings-popup")

  <!-- END MODAL-->

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <!-- <script src="js/demo/chart-pie-demo.js"></script> -->
  <script src="js/safr/main.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script>
    oTable = $('#anprDataTable').DataTable();   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
    $('#searchNumber').keyup(function(){
          //oTable.search($(this).val()).draw() ;
          oTable.column(0).search($(this).val()).draw();
    })
  </script>

</body>

</html>
