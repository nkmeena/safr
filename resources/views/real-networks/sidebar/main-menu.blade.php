<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/"><img src="img/logo.png"></a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/dashboard"><i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">Interface</div>
      @foreach ($sidebar as $menu)
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#{{str_replace(' ', '_', data_get($menu , "name" , ""))}}" aria-expanded="true" aria-controls="collapseOne"><img src="{{data_get($menu , "sidebar_img")}}"><span>{{data_get($menu , "name")}}</span></a>
        @if(sizeof(data_get($menu , "subcategory" , []))>0)
        <div id="{{str_replace(' ', '_', data_get($menu , "name" , ""))}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-dark py-2 collapse-inner rounded">
            <!-- <a class="collapse-item" href="#">Face Search</a> -->
              @foreach ( data_get($menu , "subcategory" , []) as  $item)
                @if(!empty(data_get( $item , "modal" , "")))
                <a class="collapse-item setting-modal" settings-modal-label="{{ data_get($item , "modal_header") , "" }}" setting-type="{{data_get( $item , "setting_type" , "")}}" onclick="setSettingType(this)"  data-toggle="modal" data-target="#{{data_get( $item , "modal" , "")}}" data-whatever="@mdo" >{{data_get( $item , "name" , "")}}</a>
                @else
                <a class="collapse-item" href="{{data_get( $item , "url" , "#")}}">{{data_get( $item , "name" , "")}}</a>
                @endif
              @endforeach
          </div>
        </div>
        @endif
      </li>
      @endforeach
      <!-- Nav Item - FR -->
      
    </ul>