<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Safr - Dashboard</title>
<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200&display=swap" rel="stylesheet"> 
<!-- Custom styles for this template-->
<link href="css/admin.min.css" rel="stylesheet">
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<script>

function getImage(ele){
  var eid =  $(ele).attr('event-id');
  var pid =  $(ele).attr('person-id');
  var bgid =  $(ele).attr('background-img-id');
  
  var name =  $(ele).attr('event-name');
  var gender =  $(ele).attr('gender');
  var startTime =  $(ele).attr('start-time');
  var idClass =  $(ele).attr('idClass');
  var personType =  $(ele).attr('person-type');

  //startTime 
  $("#startTime").text(startTime);
  $("#personType").text(personType);
  $("#eName").text(name);
  $("#gender").text(gender);
  $("#idClass").text(idClass);

  $("#personIMage").attr("src","");
  $("#eventIMage").attr("src","");
  $("#bgIMage").attr("src","");

  // $.ajax({
  //   url: "/image/"+pid+"?type=person", 
  //   // async: false,
  //   success: function(result) {
  //       $("#personIMage").attr("src","data:image/jpeg;base64,"+result);
  //       $("#myModal").modal("toggle");
  //   }});

    ajaxCall( "/image/"+eid+"?type=event" , "event");
    ajaxCall( "/image/"+pid+"?type=person" , "person");
    ajaxCall( "/image/"+bgid+"?type=background" , "background");

    $("#myModal").modal("toggle");
}

function ajaxCall(apiUrl , type){
  $.ajax({
    url: apiUrl, 
    // async: false,
    success: function(result) {
        if(type=="person"){
          $("#personIMage").attr("src","data:image/jpeg;base64,"+result);
        }else if(type=="event"){
          $("#eventIMage").attr("src","data:image/jpeg;base64,"+result);
        } else if(type=="background"){
          $("#bgIMage").attr("src","data:image/jpeg;base64,"+result);
        }

    }});
}



</script>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    @include("real-networks.sidebar.main-menu")
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <!-- START of Topbar -->
      @include("real-networks.header.navbar")
       <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <!-- Face Recognition -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Face Recognition</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/frs-icon.png">
                    </div>
                  </div>

                  <div class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" id="searchName" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                 </div>
                </div>
              </div>
            </div>

            <!-- ANPR -->
            <!-- <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">ANPR</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/anpr-icon.png">
                    </div>
                  </div>

                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div> -->

            <!-- Crowd Monitoring -->
            <!-- <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Video Analytics</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/video-analysis.png">
                    </div>
                  </div>
                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div> -->
          </div> 
          <div class="dynamic-data">
            <input id="male_ratio" type="text"  value="{{data_get($gender_ratio , "male" , "")}}" class="hidden d-none">
            <input id="female_ratio" type="text"  value="{{data_get($gender_ratio , "female" , "")}}" class="hidden d-none">
            <div class="datetime_ratio_data">
              @foreach ($datetime_ratio as $key=>$value )
              <input key="{{$key}}" type="text"  value="{{$value}}" class="hidden d-none">
              @endforeach
            </div>
          </div>
          <!-- Content Row -->
          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-uppercase">Face Recognition Analytics (Last 7 Days)</h6>
                  <div class="dropdown no-arrow">
                    <!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-uppercase">Demography</h6>
                  <div class="dropdown no-arrow">
                    <!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> Male
                    </span>
                    <!-- <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> Social
                    </span> -->
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i> Female
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-uppercase">Subject Sighting Events (FR)</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="eventDataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Hours</th>
                          <th>Location</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($events as $event)
                          <tr>
                            <?php 
                            $epoch = ceil( data_get($event  ,'startTime' , 0)/1000);
                            
                            $hours = "-";
                            if(!empty($epoch)){
                              $dt = new DateTime("@$epoch");  // convert UNIX timestamp to PHP DateTime
                            //  $hours =  $dt->format('Y-m-d H:i:s'); // output = 2017-01-01 00:00:00
                            $dt->add(new DateInterval('PT5H30M'));
                            $hours = $dt->format('Y-m-d h:i A');// output = 2017-01-01 00:00:00

                            }
                            ?>
                            <td>{{data_get($event  ,'name' , "-")}}</td>
                            <td>{{$hours}}</td>
                            <td>{{data_get($event , 'siteId' , "-")}}</td>
                            <td><button class="btn btn-green btn-sm"><a href="/detail-page?personId={{data_get($event  ,"personId" , "-")}}">Options</a></button></td>
                            <td><button event-name="{{data_get($event  ,'name' , "-")}}"  event-id="{{data_get($event  ,'eventId' , "-")}}" gender="{{data_get($event  ,'gender' , "-")}}" start-time="{{$hours}}" idClass="{{data_get($event  ,'idClass' , "-")}}" background-img-id="{{data_get($event  ,'eventId' , "-")}}" person-type="{{data_get($event  ,'type' , "-")}}" type="person" person-id="{{data_get($event  ,"personId" , "-")}}" class="btn btn-dark btn-sm"  onclick="getImage(this)">Details</button></td>
                          </tr>
                        @endforeach
                        
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div><!-- End of Main Content -->

      <!-- Footer -->
      @include("real-networks.footer.footer")
      <!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  @include("real-networks.popup.logout")
  <!-- END Logout Modal-->
  
   <!-- Event Modal -->
   @include("real-networks.popup.event-detail")
  
  <!-- Settings modal --->

  @include("real-networks.popup.settings-popup")

  <!-- END MODAL-->

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  <script src="js/safr/main.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script>
    oTable = $('#eventDataTable').DataTable();   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
    $('#searchName').keyup(function(){
          //oTable.search($(this).val()).draw() ;
          oTable.column(0).search($(this).val()).draw();
    })
  </script>
</body>

</html>
