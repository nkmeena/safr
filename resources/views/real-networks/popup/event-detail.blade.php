<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Event Detail</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <!-- <img src="cinqueterre.jpg" id ="personIMage" class="rounded" alt="Person Image"> -->
          <table class="table table-striped">
            <tbody>

              <tr>
                <td>Name</td>
                <td id="eName">-</td>
              </tr>

              <tr>
                <td>Gender</td>
                <td id="gender">-</td>
              </tr>

              <tr>
                <td>Start Time</td>
                <td id="startTime">-</td>
              </tr>

              <tr>
                <td>Person Type</td>
                <td id="personType">-</td>
              </tr>

              <tr>
                <td>idClass </td>
                <td id="idClass">-</td>
              </tr>


              <tr>
                <td>Event Image</td>
                <td > <img width="100" height="150" src="#" id ="eventIMage" class="rounded" alt="Event Image"> </td>
              </tr>

              <tr>
                <td>Person Image</td>
                <td > <img width="100" height="150" src="#" id ="personIMage" class="rounded" alt="Person Image"> </td>
              </tr>

              <tr>
                <td>Background Image</td>
                <td > <img width="100" height="150" src="#" id ="bgImage" class="rounded" alt="Background Image"> </td>
              </tr>


            </tbody>
          </table>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>