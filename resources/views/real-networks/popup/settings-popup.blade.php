
<div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="settingsModalLabel"><center>Settings</center></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="settings-data">
        <form>
          @if(!empty($config_data ))
            @foreach ( $config_data as $key1 => $value1 )
              <div class="database-settings {{$key1}}">
                @foreach ( $value1  as $key => $value )
                <div class="form-group">
                  <label for="{{str_replace(' ', '_', $key)}}" class="col-form-label"> {{$key }}</label>
                  <input type="text" class="form-control" value="{{$value}}" key="{{$key }}" id="{{str_replace(' ', '_', $key)}}">
                </div>
                @endforeach
              </div>
            @endforeach
          @endif
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-settings" setting-type="server" onclick="saveSettings(this)">Save</button>
      </div>
    </div>
  </div>
</div>