<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>safr - Face Recognition</title>
<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200&display=swap" rel="stylesheet"> 
<!-- Custom styles for this template-->
<link href="css/admin.min.css" rel="stylesheet">
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html"><img src="img/logo.png"></a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="index.html"><i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">Interface</div>

      <!-- Nav Item - FR -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><img src="img/face-recognition-left-icon.png"><span>Face Recognition</span></a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-dark py-2 collapse-inner rounded">
            <a class="collapse-item" href="#">Face Search</a>
            <a class="collapse-item" href="#">Enroll Face</a>
            <a class="collapse-item" href="#">Events</a>
            <a class="collapse-item" href="#">Upload Video</a>
            <a class="collapse-item" href="#">Connect Database</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - ANPR -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"><img src="img/anpr-left-icon.png"><span>ANPR</span></a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-dark py-2 collapse-inner rounded">
            <a class="collapse-item" href="#">Vehicle Search</a>
            <a class="collapse-item" href="#">Number Plate Search</a>
            <a class="collapse-item" href="#">Speed Violation</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - CM -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree"><img src="img/video-analysis-left-icon.png"><span>Video Analytics</span></a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
          <div class="bg-dark py-2 collapse-inner rounded">
            <a class="collapse-item" href="#">Abonded Object</a>
            <a class="collapse-item" href="#">Intrusion</a>
            <a class="collapse-item" href="#">Camera Temporing</a>
            <a class="collapse-item" href="#">Crowd Monitor</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Cameras -->
      <li class="nav-item">
        <a class="nav-link" href="#"><img src="img/camera-left-icon.png"><span>Cameras</span></a>
      </li>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <li class="nav-item piu dropdown no-arrow text-right font-weight-bold">RealNetworks Event Analyzer <span>Account Settings</span></li>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="img/avatar-blank.jpg"></a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>
        </nav><!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Content Row -->
          <div class="row">
            <!-- Face Recognition -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Face Recognition</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/frs-icon.png">
                    </div>
                  </div>

                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <!-- ANPR -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">ANPR</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/anpr-icon.png">
                    </div>
                  </div>

                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <!-- Crowd Monitoring -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-green shadow h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center mb-3">
                    <div class="col">
                      <h6 class="font-weight-bold text-uppercase">Video Analytics</h6>
                    </div>
                    <div class="col-auto">
                      <img src="img/video-analysis.png">
                    </div>
                  </div>
                  <form class="form-card">
                    <div class="input-group mb-1 border">
                      <input type="text" class="form-control border-0" placeholder="Type Name...">
                      <div class="input-group-append border-0">
                        <button type="button" class="btn"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-uppercase">Subject Sighting Events (FR)</h6>
                </div>
                <div class="card-body">
                 
                  <div class="row">
                    <div class="col-xl-3 col-lg-3">
                      <div class="card shadow h-100">
                        <div class="card-fr-img">
                          <img src="img/avatar-1.jpg">
                          <span>7.15 PM</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-3">
                      <div class="card shadow h-100">
                        <div class="card-fr-img">
                          <img src="img/avatar-2.jpg">
                          <span>7.15 PM</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-3">
                      <div class="card shadow h-100">
                        <div class="card-fr-img">
                          <img src="img/avatar-3.jpg">
                          <span>7.15 PM</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-3">
                      <div class="card shadow h-100">
                        <div class="card-fr-img">
                          <img src="img/avatar-4.jpg">
                          <span>7.15 PM</span>
                        </div>
                      </div>
                    </div>
                  </div><!--row-->

                </div>
              </div>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div><!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>© 2021 RealNetworks, Inc. All Rights Reserved</span>
          </div>
        </div>
      </footer><!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
</body>

</html>
