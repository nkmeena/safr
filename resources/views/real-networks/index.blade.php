<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Safr - Dashboard</title>
<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200&display=swap" rel="stylesheet"> 
<!-- Custom styles for this template-->
<link href="css/admin.min.css" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    @include("real-networks.sidebar.main-menu")
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        @include("real-networks.header.navbar")
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray">Event Analyzer</h1>
          </div>

          <!-- Content Row -->
          <div class="row">
          @foreach ($sidebar as $menu)
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-bottom-green shadow h-100">
                  <div class="card-body">
                    <a href="{{data_get($menu , "url" , "#")}}" style="text-decoration: none;">
                    <div class="text-center mb-4">
                      <img src="{{data_get($menu , "dashboard_img")}}">
                      <h6 class="font-weight-bold text-uppercase mt-3">{{data_get($menu , "name")}}</h6>
                    </div>
                  </a>
              @if(sizeof(data_get($menu , "subcategory" , []))>0)
                <ul class="list-styled">
                @foreach ( data_get($menu , "subcategory" , []) as  $item)
                  @if(!empty(data_get( $item , "modal" , "")))
                  <li ><a class="setting-modal" href="#" style="text-decoration: none;" settings-modal-label="{{ data_get($item , "modal_header") , "" }}" setting-type="{{data_get( $item , "setting_type" , "")}}" onclick="setSettingType(this)"  data-toggle="modal" data-target="#{{data_get( $item , "modal" , "")}}" data-whatever="@mdo"><img src="{{data_get($item , "dashboard_img")}}">{{data_get($item , "name")}}</a></li>

                  @else
                  <li ><a href="{{data_get($item , "url")}}" style="text-decoration: none;"><img src="{{data_get($item , "dashboard_img")}}">{{data_get($item , "name")}}</a></li>

                  @endif
                @endforeach
                </ul> 
              @endif
                </div>
              </div>
            </div>
          @endforeach
            
          </div><!--/.row-->
        </div><!-- /.container-fluid -->
      </div><!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>&copy;2021 RealNetworks, Inc. All Rights Reserved.</span>
          </div>
        </div>
      </footer><!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
  @include("real-networks.popup.connect-database")
  @include("real-networks.popup.settings-popup")

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <!-- <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script> -->
  <script src="js/safr/main.js"></script>


</body>

</html>
