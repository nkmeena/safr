<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class Safer extends  Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function getIndex(){
    //     $obj = new APIRequestController();
    //     return $obj->apiRequest("get" , "app/settings" , []);
    // }

    function getIndex(){
        $sidebar = $this->getSidebar();
        $config_data = $this->getServerConfiguration();
        return view('real-networks.index' ,['sidebar'=>$sidebar , 'config_data'=>$config_data  ]);
    }
    function getLogin(){
        return view('real-networks.login');
    }
    function getRegister(){
        return view('real-networks.register');
    }

    function getForgotPassword(){
        return view('real-networks.forgot-password');
    }
    function getFaceRegognitionDetailPage(Request $request){
        $inputs = $request->all();

        $personId = $request->input('personId');

        if(empty($personId)){
            $idClass = $request->input('idClass');
            if(empty($idClass)){
                $idClass = "threat";
            }
            $inputs["idClass"]= $idClass;
        }else{
            $inputs["personId"]= $personId;
        }
        if(empty($request->input('range'))){
            $inputs["range"] = "7";
        }else{
            $inputs["range"] = $request->input('range');

        }
        $inputs["sortOrder"] = "descending";
        $inputs["combineActiveEvents"]=false;
        
        $inputs["rootEventsOnly"]=true;
        $inputs["sinceTime"]=0;
        $inputs["spanSources"]=false;
        $config_data = $this->getServerConfiguration();

        $data =  $this->saferAPIRequest("GET" , "events" , ["get"=>$inputs , "server_details"=> $config_data ]);
        $events = data_get($data , 'events' , []);
        $result = $this->getGenderRatio( $events ,  $inputs["range"] );
        $gender_ratio = data_get(  $result , "gender_ratio" , []);

        $datetime_ratio = data_get(  $result , "datetime_ratio" , []);

        
        $sidebar = $this->getSidebar();
        return view('real-networks.face-recognition.index',['events' => $events , "gender_ratio"  =>$gender_ratio , 
        "datetime_ratio"=>$datetime_ratio , "config_data"=> $config_data , 'sidebar'=>$sidebar ]);
    }
    function getFaceRecognition(){
        return view('real-networks.fr-page');
    }

    // public function getPersonPassEvents($personId , Request $request)
    // {
    //     $obj = new APIRequestController();
    //     $inputs = [];
        
    //     // $idClass = $request->input('idClass');
    //     // if(empty($idClass)){
    //     //     $idClass = "threat";
    //     // }
    //     $inputs["personId"]=$personId;
    //     $inputs["combineActiveEvents"]=false;
    //     // $inputs["idClass"]= $idClass;
    //     $inputs["rootEventsOnly"]=true;
    //     $inputs["sinceTime"]=0;
    //     $inputs["spanSources"]=false;
    //     $data =  $obj->saferAPIRequest("GET" , "events" , ["get"=>$inputs]);
    //     return view('real-networks.detail-page',['events' => data_get($data , 'events' , [])]);
    // }

    function apiTest(){
        try{
        $inputs = [];
        $inputs["combineActiveEvents"]=false;
        $inputs["idClass"]="threat";
        $inputs["rootEventsOnly"]=true;
        $inputs["sinceTime"]=0;
        $inputs["spanSources"]=false;
        return $this->saferAPIRequest("GET" , "events" , ["get"=>$inputs]);
        }catch(Exception $e){
            throw $e;
        }
    }
    function getGenderRatio($events , $range){
        $result = [];
        $result["gender_ratio"]["female"] = 0;
        $result["gender_ratio"]["male"] = 0;
        $dates = $this->getDateRange($range);

        foreach($events as $event){
            $epoch = ceil( data_get($event  ,'startTime' , 0)/1000);
            if(!empty($epoch)){
                $dt = new DateTime("@$epoch");  // convert UNIX timestamp to PHP DateTime
                $date  =  $dt->format('Y-m-d'); // output = 2017-01-01
                if(Arr::has($dates , $date)){
                    $dates[$date]++;
                }
            }
           
            if(strtolower(data_get($event ,"gender" , ""))=="male"){
                $result["gender_ratio"]["male"]++;
            }else if(strtolower(data_get($event ,"gender" , ""))=="female"){
                $result["gender_ratio"]["female"]++;
            }
        }
        $result["datetime_ratio"] = $dates;
        return $result;
    }


}
