<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Exception;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saferAPIRequest($method , $api , $data)
    {
        $domain = $this->getServerDetail("Event_Server" ,  data_get($data , "server_details"  , [])) ;
//$domain = env("SAFER_URL" , ""); // will cofigure this  
       // $userId = env("SAFER_USER_ID" , "");
        $userId  = $this->getServerDetail("User_ID" ,  data_get($data , "server_details"  , [])) ;
        $password = $this->getServerDetail("Password" ,  data_get($data , "server_details"  , [])) ;
        $directory = $this->getServerDetail("User_Directory" ,  data_get($data , "server_details"  , [])) ;

      //  $password = env("SAFER_PASS" , "");      
        $get = data_get($data, 'get', []);
        $api = (!empty($get)) ? $api . '?' . http_build_query($get) : $api;

        $curl = curl_init();
        $api_url =  $domain.$api;
        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => array(
            'accept: application/json;charset=UTF-8',
            'X-RPC-DIRECTORY: '. $directory,
            'X-RPC-AUTHORIZATION: '.$userId.':'.$password
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response , true);


    }

    public function vmsAPIRequest($method , $api , $data)
    {
        $domain  = $this->getServerDetail("Virga_Server" ,  data_get($data , "server_details"  , [])) ;
        $password = $this->getServerDetail("Password" ,  data_get($data , "server_details"  , [])) ;
        $userId  = $this->getServerDetail("User_ID" ,  data_get($data , "server_details"  , [])) ;
      
        $directory = $this->getServerDetail("User_Directory" ,  data_get($data , "server_details"  , [])) ;
        //$domain = env("VMS_SERVER_ADDRESS" , ""); // will cofigure this        
       // $userId = env("SAFER_USER_ID" , "");
        //$password = env("SAFER_PASS" , "");      
        $get = data_get($data, 'get', []);
        $api = (!empty($get)) ? $api . '?' . http_build_query($get) : $api;

        $curl = curl_init();
        $api_url =  $domain.$api;
        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => array(
            'accept: application/json;charset=UTF-8',
            'X-RPC-DIRECTORY: '.$directory,
            'X-RPC-AUTHORIZATION: '.$userId.':'.$password
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response , true);
    }

    public function anprAPIRequest($method , $api , $data)
    {
        $domain  = $this->getServerDetail("ANPR_Server" ,  data_get($data , "server_details"  , [])) ;
        $port = $this->getServerDetail("ANPR_Port" ,  data_get($data , "server_details"  , [])) ;
       
        $domain = env("ANPR_SERVER_ADDRESS" , "http://phantom.vehant.in"); // will cofigure this
        $port = env("ANPR_SERVER_PORT" , "55580"); 
        $get = data_get($data, 'get', []);
        $api = (!empty($get)) ? $api . '?' . http_build_query($get) : $api;

        $curl = curl_init();
        
        $domain = $domain.":".$port;
        $api_url =  $domain."/".$api;
    
        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        // CURLOPT_HTTPHEADER => array(
        //     'accept: application/json;charset=UTF-8',
        //     'X-RPC-DIRECTORY: main',
        //     'X-RPC-AUTHORIZATION: '.$userId.':'.$password
        // ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response , true);
    }

    public function imageAPIRequest($method , $api , $data)
    {
        $server_details = $this->getServerConfiguration();
        $domain = $this->getServerDetail("Object_Server" ,  $server_details ) ;
        //= env("OBJECT_SERVER_API" , ""); // will cofigure this
        $userId = $this->getServerDetail("User_ID" ,  $server_details ) ;
        //env("SAFER_USER_ID" , "");
        $password = $this->getServerDetail("Password" ,  $server_details ) ;
        $directory = $this->getServerDetail("User_Directory" ,  $server_details ) ;
        //env("SAFER_PASS" , "");      
        $get = data_get($data, 'get', []);
        $api = (!empty($get)) ? $api . '?' . http_build_query($get) : $api;

        $curl = curl_init();
        $api_url =  $domain.$api;
        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => array(
            'accept: */*',
            'X-RPC-DIRECTORY: '.$directory,
            'X-RPC-AUTHORIZATION: '.$userId.':'.$password
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return base64_encode($response);
    }
    
    public function getSidebar(){
        $server_config = $this->getServerConfiguration();
        $domain = data_get( $server_config , "safer_server_config.Server_Environment" , []);
        return array( 
            array( "name"=> "Face Recognition" , "url" => "/face-recognition" , "sidebar_img"=>"img/face-recognition-left-icon.png" , "dashboard_img"=>"img/frs-icon.png" , 
            "subcategory"=> array(
                array("name"=> "Enroll Face" , "url"=>$domain."/people" , "sidebar_img"=>"" , "dashboard_img"=>"img/face-enroll-icon.png" ),
                array("name"=> "Events" , "url"=>$domain."/events" , "sidebar_img"=>"" , "dashboard_img"=>"img/event-icon.png" ),
                array("name"=> "Upload Video" , "url"=>"#" , "sidebar_img"=>"" , "dashboard_img"=>"img/video-upload-icon.png" ),
                array("name"=> "Connect Database" ,"modal_header"=> "Face Recognition Settings" , "setting_type"=> "safer_server_config" ,  "url"=>"#" , "modal"=> "settingsModal" , "sidebar_img"=>"" , "dashboard_img"=>"img/database-connect.png" ),
                 )
        ),
        array( "name"=> "ANPR" , "url"=>"/anpr" , "sidebar_img"=>"img/anpr-left-icon.png" , "dashboard_img"=>"img/anpr-icon.png" , 
            "subcategory"=> array(
                // array("name"=> "Vehicle Search" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/Vehicle-Search-icon.png" ),
                array("name"=> "Number Plate Search" , "url"=>"/anpr" , "sidebar_img"=>"" , "dashboard_img"=>"img/number-plate-search-icon.png" ),
                array("name"=> "Speed Violation" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/Speed-Violation-icon.png" ),
                array("name"=> "Connect Database" , "modal_header"=> "ANPR Settings" , "setting_type" =>"anpr_server_config", "url"=>"#" , "modal"=> "settingsModal" , "sidebar_img"=>"" , "dashboard_img"=>"img/database-connect.png" ),
                ) 
        ),
        array( "name"=> "VIDEO ANALYTICS" , "sidebar_img"=>"img/video-analysis-left-icon.png" , "dashboard_img"=>"img/video-analysis.png" , 
            "subcategory"=> array(
                array("name"=> "Abonded Object" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/Abonded-Object.png" ),
                array("name"=> "Intrusion" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/Intrusion-icon.png" ),
                array("name"=> "Camera Tempering" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/Camera-Temporing.png" ),
                array("name"=> "Crowd Monitor" , "url"=>"" , "sidebar_img"=>"" , "dashboard_img"=>"img/CROWD-MONITORING.png" ),

                ) 
    ),
    array( "name"=> "CAMERAS" , "sidebar_img"=>"img/camera-left-icon.png" , "dashboard_img"=>"img/cctv-icon.png" , 
            "subcategory"=> 
            array( ) 
            )
         );
    }

    public function getServerConfiguration(){
        $settings = [];
        try{
            $default_settings = $this->getDefaultSettings();
            $myfile = fopen("server-config/server.json", "r");
            $config =  fgets($myfile);

            if(!empty($config)){
                $settings = json_decode($config , true);
                if(empty($settings["safer_server_config"])){
                    $settings["safer_server_config"] = $default_settings["safer_server_config"];
                }
                if(empty($settings["anpr_server_config"])){
                    $settings["anpr_server_config"] = $default_settings["anpr_server_config"];
                }
                if(empty($settings["video_server_config"])){
                    $settings["video_server_config"] = $default_settings["video_server_config"];
                }
            }else{
                $settings = $default_settings;
            }
        }catch(Exception $e){

        }
        
        return $settings;
    }

    public function saveServerConfiguration(Request $request , $type){
       
        /*

        $inputs = $request->all();
        $config_data = json_encode($inputs);
        file_put_contents(base_path('resources/lang/en.json'), stripslashes($config_data));

        */
        $inputs = $request->all();
        $config =  $inputs ;
        
        $myfile = fopen("server-config/server.json", "r");
        $existing_data =  fgets($myfile);

        $config_data = [];
        if(!empty( $existing_data)){
            $existing_data = json_decode($existing_data , true);
            $existing_data[$type] =  $inputs;
            $config_data   = $existing_data;
        }else{
            $config_data[$type] =  $inputs;
        }
        

        $myfile = fopen("server-config/server.json", "w");
        $config_data = json_encode($config_data);
        // return  $config_data;
        fwrite($myfile, $config_data);
        fclose($myfile);
        return true;
    }

    function getImage($id, Request $request){
        // $id = $request->input('id');
         $type = $request->input('type');
         $inputs = [];
         $api  = "";
         if($type=="person"){
             $api  = 'person/'.$id.'/face' ;
         }else if($type=="event"){
             $api  = 'obj/'.base64_encode($id).'/face' ;
         }else if($type=="background"){
             $api  = 'obj/'.base64_encode($id).'/sceneThumb' ;
         }
         return $this->imageAPIRequest("GET" , $api , ["get"=>$inputs]);
     }

     function getDateRange($range){
        config(['app.timezone' => 'America/Chicago']);
        $now = Carbon::now();
        $weekStartDate = $now->subDays($range)->format('Y-m-d');
        $now = Carbon::now();
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        //dd($weekStartDate);
        //dd($weekEndDate);
        // if($type=="7days"){

        // }else if($type=="months"){

        // }else{

        // }
        $period = CarbonPeriod::create($weekStartDate , $weekEndDate);
        // Iterate over the period
        $dates = [];
        foreach ($period as $date) {
            $dates[$date->format('Y-m-d')]  = 0;
        }
        return $dates;
    }

    public function getDefaultSettings()
    {
        $safer_server = env("SAFER_URL" , "");
        $userId = env("SAFER_USER_ID" , "");
        $password = env("SAFER_PASS" , "");  
        $object_server = env("OBJECT_SERVER_API" , "https://cvos.int2.real.com");
        $anpr_server =  env("ANPR_SERVER_ADDRESS" , "http://phantom.vehant.in");
        $anpr_server_port =  env("ANPR_SERVER_PORT" , "55580");
        return array( 
                    "safer_server_config" => array ("User ID" => $userId ,
                    "Password" =>  $password ,
                      "User Directory" => "main" , 
                      "Server Environment"=> "localhost" ,
                      "Covi Server"=>"" ,
                      "Event Server"=>$safer_server , 
                      "Object Server"=>  $object_server,
                      "Virga Server"=>"" ,),

                      "anpr_server_config" => array( 
                        "ANPR Server"=> $anpr_server,
                        "ANPR Port"=> $anpr_server_port
                      ),
                      "video_server_config" => array()  
                    );

    }
    public function getDefaultDatabaseSettings()
    {
        return array("Database Server"=> "localhost" , "Database Username"=> "" , "Database Password" => "" , "Database Name"=> "");
    }

    public function getServerDetail($key , $serverDetails){
        foreach($serverDetails as $server){
            foreach($server as $sKey => $sValue){
                if($sKey == $key){
                    return $sValue;
                }
            }
        }
        return "";
    }
}
